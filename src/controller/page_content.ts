import { Request, Response } from "express";
import { NotFoundError } from "../errors/not-found-error";
import { PageContent } from "../models/page_content";

export const create = async (req: Request, res: Response) => {
  const { page, key, type, content, label } = req.body;

  let page_content_file = null;
  const { content_file } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (content_file && content_file[0]) {
    page_content_file = content_file[0].path;
  }

  const testimony = await PageContent.create({
    page,
    key,
    type,
    content,
    label,
    file: page_content_file,
  });

  res.status(200).json(testimony);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_page_content = await PageContent.findByPk(id);
  if (!existing_page_content) {
    throw new NotFoundError();
  }

  let page_content_file = null;
  const { content_file } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (content_file && content_file[0]) {
    page_content_file = content_file[0].path;
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_page_content.set(key, req.body[key]);
    }
  }

  if (page_content_file) {
    existing_page_content.set("file", page_content_file);
  }
  await existing_page_content.save();

  res.status(200).json(existing_page_content);
};

// export const delete_testimony = async (req: Request, res: Response) => {
//   const { id } = req.params;

//   const existing_testimony = await Testimony.findByPk(id);

//   if (!existing_testimony) {
//     throw new NotFoundError();
//   }

//   existing_testimony.destroy();

//   res.status(200).json({ message: "Success deleted!" });
// };
