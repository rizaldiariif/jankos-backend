import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { BannerHome } from "../models/banner_home";
import { ProductImage } from "../models/product_image";

export const create = async (req: Request, res: Response) => {
  const { product_id } = req.body;

  let product_image_image = null;
  const { image } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image && image[0]) {
    product_image_image = image[0].path;
  }

  if (!product_image_image) {
    throw new BadRequestError("Image File is not completed!");
  }

  const product_image = await ProductImage.create({
    product_id,
    url: product_image_image,
  });

  res.status(200).json(product_image);
};

export const delete_product_image = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_product_image = await ProductImage.findByPk(id);

  if (!existing_product_image) {
    throw new NotFoundError();
  }

  existing_product_image.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
