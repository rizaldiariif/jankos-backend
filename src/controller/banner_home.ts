import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { BannerHome } from "../models/banner_home";

export const create = async (req: Request, res: Response) => {
  let testimony_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    testimony_image_thumbnail = image_thumbnail[0].path;
  }

  if (!testimony_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const banner_home = await BannerHome.create({
    thumbnail: testimony_image_thumbnail,
  });

  res.status(200).json(banner_home);
};

export const delete_banner_home = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_banner_home = await BannerHome.findByPk(id);

  if (!existing_banner_home) {
    throw new NotFoundError();
  }

  existing_banner_home.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
