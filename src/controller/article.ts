import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { Article } from "../models/article";

export const create = async (req: Request, res: Response) => {
  const { title, text, author } = req.body;

  let article_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    article_thumbnail = image_thumbnail[0].path;
  }

  if (!article_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const article = await Article.create({
    title,
    text,
    author,
    thumbnail: article_thumbnail,
  });

  res.status(200).json(article);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_article = await Article.findByPk(id);
  if (!existing_article) {
    throw new NotFoundError();
  }

  let article_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    article_thumbnail = image_thumbnail[0].path;
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_article.set(key, req.body[key]);
    }
  }

  if (article_thumbnail) {
    existing_article.set("thumbnail", article_thumbnail);
  }
  await existing_article.save();

  res.status(200).json(existing_article);
};

export const delete_article = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_article = await Article.findByPk(id);

  if (!existing_article) {
    throw new NotFoundError();
  }

  existing_article.destroy();

  res.status(200).json({ message: "Success deleted!" });
};

export const listing_slugs = async (req: Request, res: Response) => {
  const articles = await Article.findAll({ attributes: ["slug"], limit: 1000 });

  res.status(200).json(articles);
};

export const get_detail = async (req: Request, res: Response) => {
  const { slug } = req.params;

  const article = await Article.findOne({ where: { slug } });

  if (!article) {
    throw new NotFoundError();
  }

  res.status(200).json(article);
};

export const get_recent_articles = async (_req: Request, res: Response) => {
  const articles = await Article.findAll({
    order: [["createdAt", "DESC"]],
    limit: 5,
  });

  res.status(200).json(articles);
};
