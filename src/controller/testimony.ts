import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { Testimony } from "../models/testimony";

export const create = async (req: Request, res: Response) => {
  const { name, text } = req.body;

  let testimony_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    testimony_image_thumbnail = image_thumbnail[0].path;
  }

  if (!testimony_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const testimony = await Testimony.create({
    name,
    text,
    thumbnail: testimony_image_thumbnail,
  });

  res.status(200).json(testimony);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_testimony = await Testimony.findByPk(id);
  if (!existing_testimony) {
    throw new NotFoundError();
  }

  let testimony_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    testimony_image_thumbnail = image_thumbnail[0].path;
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_testimony.set(key, req.body[key]);
    }
  }

  if (testimony_image_thumbnail) {
    existing_testimony.set("thumbnail", testimony_image_thumbnail);
  }
  await existing_testimony.save();

  res.status(200).json(existing_testimony);
};

export const delete_testimony = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_testimony = await Testimony.findByPk(id);

  if (!existing_testimony) {
    throw new NotFoundError();
  }

  existing_testimony.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
