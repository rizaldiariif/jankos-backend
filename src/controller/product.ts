import { Request, Response } from "express";
import { Op } from "sequelize";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { Product } from "../models/product";
import { ProductLevelPrice } from "../models/produt_level_price";

export const create = async (req: Request, res: Response) => {
  const {
    name,
    price,
    weight,
    width,
    height,
    length,
    description,
    product_category_id,
    is_package,
    is_starter,
  } = req.body;

  let product_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    product_image_thumbnail = image_thumbnail[0].path;
  }

  if (!product_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const product = await Product.create({
    name,
    price,
    weight,
    width,
    height,
    length,
    description,
    thumbnail: product_image_thumbnail,
    product_category_id,
    is_package,
    is_starter,
  });

  // if (is_starter) {
  //   await Product.update(
  //     { is_starter: false },
  //     {
  //       where: {
  //         id: {
  //           [Op.ne]: product.id,
  //         },
  //       },
  //     }
  //   );
  // }

  res.status(200).json(product);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_product = await Product.findByPk(id);
  if (!existing_product) {
    throw new NotFoundError();
  }

  let product_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    product_image_thumbnail = image_thumbnail[0].path;
  }

  const { product_level_prices } = req.body;
  console.log({ product_level_prices });
  delete req.body.product_level_prices;

  for (const key of Object.keys(req.body)) {
    if (req.body[key] || req.body[key] === false) {
      existing_product.set(key, req.body[key]);
    }
  }

  if (product_image_thumbnail) {
    existing_product.set("thumbnail", product_image_thumbnail);
  }
  await existing_product.save();

  for (const level_price of product_level_prices) {
    const [id, price] = level_price.split("_");
    await ProductLevelPrice.update(
      {
        price,
      },
      { where: { id } }
    );
  }

  // if (existing_product.is_starter) {
  //   await Product.update(
  //     { is_starter: false },
  //     {
  //       where: {
  //         id: {
  //           [Op.ne]: existing_product.id,
  //         },
  //       },
  //     }
  //   );
  // }

  res.status(200).json(existing_product);
};

export const delete_product = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_product = await Product.findByPk(id);

  if (!existing_product) {
    throw new NotFoundError();
  }

  existing_product.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
