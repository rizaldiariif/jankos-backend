import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { AdminLevel } from "../models/admin_level";

export const create = async (req: Request, res: Response) => {
  const { name, hierarchy_number } = req.body;

  const admin_level = await AdminLevel.create({
    name,
    hierarchy_number,
  });

  res.status(200).json(admin_level);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_admin_level = await AdminLevel.findByPk(id);
  if (!existing_admin_level) {
    throw new NotFoundError();
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_admin_level.set(key, req.body[key]);
    }
  }

  await existing_admin_level.save();

  res.status(200).json(existing_admin_level);
};

export const delete_admin_level = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_admin_level = await AdminLevel.findByPk(id);

  if (!existing_admin_level) {
    throw new NotFoundError();
  }

  if (
    existing_admin_level.hierarchy_number === 1 ||
    existing_admin_level.hierarchy_number === 999
  ) {
    throw new BadRequestError("Cannot delete basic admin level!");
  }

  existing_admin_level.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
