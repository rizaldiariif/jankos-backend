import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { AdminLevel } from "../models/admin_level";
import { MemberLevel } from "../models/member_level";

export const create = async (req: Request, res: Response) => {
  const {
    name,
    minimum_order,
    hierarchy_number,
    commision_percentage,
    price_cut_percentage,
  } = req.body;

  const member_level = await MemberLevel.create({
    name,
    hierarchy_number,
    minimum_order,
    commision_percentage,
    price_cut_percentage,
  });

  res.status(200).json(member_level);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_member_level = await MemberLevel.findByPk(id);
  if (!existing_member_level) {
    throw new NotFoundError();
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_member_level.set(key, req.body[key]);
    }
  }

  await existing_member_level.save();

  res.status(200).json(existing_member_level);
};

export const getById = async (req: Request, res: Response) => {
  const { id } = req.params;

  const member_level = await MemberLevel.findByPk(id);

  if (!member_level) {
    throw new NotFoundError();
  }

  res.status(200).json(member_level);
};

// export const delete_admin_level = async (req: Request, res: Response) => {
//   const { id } = req.params;

//   const existing_admin_level = await AdminLevel.findByPk(id);

//   if (!existing_admin_level) {
//     throw new NotFoundError();
//   }

//   if (
//     existing_admin_level.hierarchy_number === 1 ||
//     existing_admin_level.hierarchy_number === 999
//   ) {
//     throw new BadRequestError("Cannot delete basic admin level!");
//   }

//   existing_admin_level.destroy();

//   res.status(200).json({ message: "Success deleted!" });
// };
