import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotAuthorizedError } from "../errors/not-authorized-error";
import { NotFoundError } from "../errors/not-found-error";
// import { geocoder } from "../helpers/api/geocoder";
import {
  checkAvailableCouriers,
  checkServicePrice,
  shipdeoClient,
} from "../helpers/api/shipdeo";
import { snapClient } from "../helpers/api/snap";
import { tokenize } from "../helpers/function/jwt";
import { PasswordClient } from "../helpers/function/password-client";
import { Member } from "../models/member";
import { MemberBankAccount } from "../models/member_bank_account";
import { MemberLevel } from "../models/member_level";
import { MemberPointLedger } from "../models/member_point_ledger";
import { MemberReferral } from "../models/member_referral";
import { MemberReferralLedger } from "../models/member_referral_ledger";
import { MemberShippingAddress } from "../models/member_shipping_address";
import { Order } from "../models/order";
import {
  OrderDetail,
  OrderDetailCreationAttributes,
} from "../models/order_detail";
import { OrderStatus } from "../models/order_status";
import { Product } from "../models/product";
import { addHours, format } from "date-fns";
import { irisCreatorClient } from "../helpers/api/iris";
import { MemberWithdrawalLedger } from "../models/member_withdrawal_ledger";
import { OrderStatusHistory } from "../models/order_status_history";
import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import { MailOptions } from "nodemailer/lib/json-transport";
import { Op } from "sequelize";
import { generate_user_invoice_email } from "../helpers/emails/user_invoice_email";

export const singup = async (req: Request, res: Response) => {
  const {
    name,
    phone,
    email,
    password,
    bank_name,
    bank_account_name,
    bank_account_number,
    address,
    member_level_id,
    shipping_address,
    shipping_subdistrict_code,
    shipping_postal_code,
    shipping_courier_code,
    shipping_courier_service,
    referral_code,
    products,
    social_media_url,
    marketplace_url,
    shipping_type,
  } = req.body;

  let member_image_id_card = null;

  const existing_member = await Member.findOne({ where: { email } });
  if (existing_member) {
    throw new BadRequestError("Email is already registered");
  }

  const shipdeo_client = await shipdeoClient();

  const { image_id_card } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_id_card && image_id_card[0]) {
    member_image_id_card = image_id_card[0].path;
  }

  if (!member_image_id_card) {
    throw new BadRequestError("Image File is not completed!");
  }

  const selected_member_level = await MemberLevel.findOne({
    where: { id: member_level_id },
  });

  if (!selected_member_level) {
    throw new BadRequestError("Invalid Member Level");
  }

  const member = await Member.create({
    name,
    email,
    phone,
    password,
    address,
    member_level_id: member_level_id,
    image_id_card: member_image_id_card,
    social_media_url,
    marketplace_url,
  });

  await MemberBankAccount.create({
    bank_name,
    bank_account_name,
    bank_account_number,
    member_id: member.id,
  });

  const {
    data: { data: shipdeo_address },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: shipping_subdistrict_code,
  });

  // const [geocode_address] = await geocoder.geocode({
  //   // address: `${shipping_address}, ${shipdeo_address.subdistrict_name}, ${shipdeo_address.city.city_name}, ${shipdeo_address.city.province.province_name}`,
  //   country: "Indonesia",
  //   zipcode: `${shipping_postal_code}`,
  // });

  const created_shipping_address = await MemberShippingAddress.create({
    contact_address: shipping_address,
    member_id: member.id,
    address_name: "Main",
    contact_name: member.name,
    contact_phone: member.phone,
    contact_email: member.email,
    province_code: shipdeo_address.city.province.province_code,
    province_name: shipdeo_address.city.province.province_name,
    city_code: shipdeo_address.city.city_code,
    city_name: shipdeo_address.city.city_name,
    subdistrict_code: shipdeo_address.subdistrict_code,
    subdistrict_name: shipdeo_address.subdistrict_name,
    postal_code: shipping_postal_code,
    // latitude: `${geocode_address.latitude}`,
    // longitude: `${geocode_address.longitude}`,
  });

  // NANTI DIUNCOMMENT KALO UDH MAU PENERAPAN FITUR
  // if (referral_code) {
  //   const upline_member = await Member.findOne({ where: { referral_code } });

  //   if (upline_member) {
  //     await MemberReferral.create({
  //       referral_code,
  //       upline_member_id: upline_member.id,
  //       referred_member_id: member.id,
  //     });
  //   }
  // }

  const order_status = await OrderStatus.findOne({
    where: { code: "menunggu_pembayaran" },
  });

  if (!order_status) {
    throw new Error("Order Status is invalid!");
  }

  const selected_products: {
    product: Product;
    quantity: number;
  }[] = [];

  for (const iterator of JSON.parse(products) as {
    product_id: string;
    quantity: number;
  }[]) {
    const product = await Product.findByPk(iterator.product_id, {
      include: ["product_level_prices"],
    });

    if (!product) {
      throw new BadRequestError("Product not found!");
    }

    selected_products.push({
      product,
      quantity: iterator.quantity,
    });
  }

  const shipping_data: {
    shipping_courier_code: string | undefined;
    shipping_courier_service: string | undefined;
    shipping_price: number;
  } = {
    shipping_courier_code: undefined,
    shipping_courier_service: undefined,
    shipping_price: 0,
  };

  if (shipping_type !== "pickup") {
    const shipping_service = await checkServicePrice(
      created_shipping_address,
      selected_products,
      shipping_courier_code,
      shipping_courier_service
    );

    shipping_data.shipping_courier_code = shipping_service.courierCode;
    shipping_data.shipping_courier_service = shipping_service.service;
    shipping_data.shipping_price = shipping_service.price;
  }

  const order = await Order.create({
    member_id: member.id,
    contact_address: created_shipping_address.contact_address,
    contact_name: created_shipping_address.contact_name,
    contact_phone: created_shipping_address.contact_phone,
    contact_email: member.email,
    order_status_id: order_status.id,
    province_code: created_shipping_address.province_code,
    province_name: created_shipping_address.province_name,
    city_code: created_shipping_address.city_code,
    city_name: created_shipping_address.city_name,
    subdistrict_code: created_shipping_address.subdistrict_code,
    subdistrict_name: created_shipping_address.subdistrict_name,
    postal_code: created_shipping_address.postal_code,
    // latitude: created_shipping_address.latitude,
    // longitude: created_shipping_address.longitude,
    shipping_type: shipping_type,
    address_note: created_shipping_address.note,
    ...shipping_data,
  });

  let midtrans_price = 0;
  midtrans_price += order.shipping_price;

  for (const iterator of selected_products) {
    const order_detail = await OrderDetail.create({
      order_id: order.id,
      product_id: iterator.product.id,
      name: iterator.product.name,
      description: iterator.product.description,
      quantity: iterator.quantity,
      thumbnail: iterator.product.thumbnail,
      weight: iterator.product.weight,
      height: iterator.product.height,
      length: iterator.product.length,
      width: iterator.product.width,
      price: iterator.product.price,
      final_weight: iterator.product.weight,
      // @ts-ignore
      final_price: iterator.product.product_level_prices?.find(
        (plp: any) => plp.member_level_id === member_level_id
      )?.price,
    });
    midtrans_price += order_detail.final_price * order_detail.quantity;
  }

  const response_snap = await snapClient.post(`/snap/v1/transactions`, {
    transaction_details: {
      order_id: order.id,
      gross_amount: midtrans_price,
    },
    customer_details: {
      first_name: member.name,
      email: member.email,
      phone: member.phone,
    },
    expiry: {
      unit: "hours",
      duration: 1,
    },
  });

  order.set("payment_url", response_snap.data.redirect_url);
  await order.save();

  if (typeof process.env.NODEMAILER_USER !== "string") {
    throw new Error("NODEMAILER_USER must be defined.");
  }
  if (typeof process.env.NODEMAILER_PASS !== "string") {
    throw new Error("NODEMAILER_PASS must be defined.");
  }

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS,
    },
  });

  const mailOptions = {
    from: process.env.NODEMAILER_USER,
    to: "jankosglowindonesia@gmail.com",
    subject: `Pesanan Baru - ${member.name}`,
    html: `
    <p>Telah masuk order baru oleh user ${member.name}.</p>
    <p>Detail Pesanan Dapat dilihat pada link berikut.</p>
    <br/>
    <a href="${process.env.APP_CLIENT_HOST}/cms/order/${order.id}">Click Here</a>
    <br/>
    <p>Atau salin link berikut</p>
    <p>${process.env.APP_CLIENT_HOST}/cms/order/${order.id}</p>
    `,
  } as MailOptions;

  await transporter.sendMail(mailOptions);

  // res.status(200).json({ message: "Success Registered!" });
  // Generate token
  const token = tokenize({
    id: member.id,
    email: member.email,
  });

  // Assign token to session
  res.status(200).send({ jwt: token });
};

export const listing_order = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const existing_member = await Member.findOne({ where: { email: email } });

  if (!existing_member) {
    throw new BadRequestError("Invalid credentials.");
  }

  const passwordMatch = await PasswordClient.compare(
    existing_member.password,
    password
  );

  if (!passwordMatch) {
    throw new BadRequestError("Invalid credentials.");
  }

  const orders = await Order.findAll({
    where: {
      member_id: existing_member.id,
    },
    include: [
      "order_status",
      "order_details",
      { association: "order_status_histories", order: { col: "createdAt" } },
    ],
  });

  res.status(200).json(orders);
};

export const listing_couriers = async (_req: Request, res: Response) => {
  const shipdeo_client = await shipdeoClient();
  const response = await shipdeo_client.get("/v1/couriers");

  res.status(200).send(response.data);
};

export const listing_subdistricts = async (_req: Request, res: Response) => {
  const shipdeo_client = await shipdeoClient();
  const response = await shipdeo_client.get("/v1/master/locations");

  res.status(200).send(response.data);
};

export const listing_locations = async (req: Request, res: Response) => {
  const { name, type } = req.query;

  const shipdeo_client = await shipdeoClient();
  const response = await shipdeo_client.post("/v1/master/locations", {
    name: name || "",
    type: type || "province",
  });

  res.status(200).send(response.data);
};

export const listing_postal_codes = async (req: Request, res: Response) => {
  const { name } = req.query;

  const shipdeo_client = await shipdeoClient();
  const response = await shipdeo_client.post(
    "/v1/master/locations/postalcode",
    {
      name,
    }
  );

  res.status(200).send(response.data);
};

export const check_shipping_price_signup = async (
  req: Request,
  res: Response
) => {
  if (typeof process.env.SHIPDEO_JANKOS_COMPLETE_ADDRESS !== "string") {
    throw new Error("SHIPDEO_JANKOS_COMPLETE_ADDRESS is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_POSTAL_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_POSTAL_CODE is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_SUBDISTRICT_CODE is not set!");
  }

  const {
    // product_id,
    // quantity,
    destination_subdistrict_code,
    destination_postal_code,
    products,
  } = req.body;

  // const product = await Product.findOne({ where: { id: product_id } });

  // if (!product) {
  //   throw new BadRequestError("Product is invalid!");
  // }

  const shipdeo_client = await shipdeoClient();

  // const [geocode_response_destination] = await geocoder.geocode(
  //   destination_complete_address
  // );
  // const [geocode_response_jankos] = await geocoder.geocode(
  //   process.env.SHIPDEO_JANKOS_COMPLETE_ADDRESS
  // );

  const {
    data: { data: address_destination },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: destination_subdistrict_code,
  });
  const {
    data: { data: address_jankos },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE,
  });

  const {
    data: { data: couriers },
  } = await shipdeo_client.get("/v1/couriers");

  const selected_products: {
    product: Product;
    quantity: number;
  }[] = [];

  for (const iterator of JSON.parse(products) as {
    product_id: string;
    quantity: number;
  }[]) {
    const product = await Product.findByPk(iterator.product_id);

    if (!product) {
      throw new BadRequestError("Product not found!");
    }

    selected_products.push({
      product,
      quantity: iterator.quantity,
    });
  }

  const { data: pricing } = await shipdeo_client.post("/v1/couriers/pricing", {
    couriers: couriers.list.map((courier: any) => courier.code),
    is_cod: false,
    // origin_lat: geocode_response_jankos.latitude,
    // origin_long: geocode_response_jankos.longitude,
    origin_province_code: address_jankos.city.province.province_code,
    origin_province_name: address_jankos.city.province.province_name,
    origin_city_code: address_jankos.city.city_code,
    origin_city_name: address_jankos.city.city_name,
    origin_subdistrict_code: address_jankos.subdistrict_code,
    origin_subdistrict_name: address_jankos.subdistrict_name,
    origin_postal_code: process.env.SHIPDEO_JANKOS_POSTAL_CODE,
    // destination_lat: geocode_response_destination.latitude,
    // destination_long: geocode_response_destination.longitude,
    destination_province_code: address_destination.city.province.province_code,
    destination_province_name: address_destination.city.province.province_name,
    destination_city_code: address_destination.city.city_code,
    destination_city_name: address_destination.city.city_name,
    destination_subdistrict_code: address_destination.subdistrict_code,
    destination_subdistrict_name: address_destination.subdistrict_name,
    destination_postal_code: destination_postal_code,
    items: selected_products.map((selected_product, index) => {
      return {
        name: selected_product.product.name,
        description: selected_product.product.description,
        weight: selected_product.product.weight,
        weight_uom: "gr",
        qty: selected_product.quantity,
        value: selected_product.product.weight * selected_product.quantity,
        width: selected_product.product.width,
        height: selected_product.product.height,
        length: selected_product.product.length,
        is_wood_package: false,
        dimension_uom: "cm",
      };
    }),
    isCallWeight: true,
  });

  const response_payload = pricing.data.filter(
    (p: any) => p.errors === undefined
  );

  res.status(200).json(response_payload);
};

export const check_shipping_price = async (req: Request, res: Response) => {
  const { items, member_shipping_address_id } = req.body;

  const cart_products: { product: Product; quantity: number }[] = [];

  for (const item of items) {
    const product = await Product.findByPk(item.product_id);

    if (!product) {
      continue;
    }

    cart_products.push({ product, quantity: item.quantity });
  }

  if (cart_products.length === 0) {
    throw new BadRequestError("Cart is empty!");
  }

  const shipping_address = await MemberShippingAddress.findByPk(
    member_shipping_address_id
  );

  if (!shipping_address) {
    throw new BadRequestError("Shipping Address is invalid!");
  }

  const available_pricing = await checkAvailableCouriers(
    shipping_address,
    cart_products
  );

  res.status(200).json(available_pricing);
};

export const my_orders = async (req: Request, res: Response) => {
  const { currentMember } = req;
  const { code } = req.query;

  if (!code || !currentMember) {
    throw new BadRequestError("Request is invalid!");
  }

  if (typeof code !== "string") {
    throw new BadRequestError("Request is invalid!");
  }

  const order_status = await OrderStatus.findOne({
    where: {
      code,
    },
  });

  if (!order_status) {
    throw new BadRequestError("Request is invalid!");
  }

  const orders = await Order.findAll({
    where: {
      member_id: currentMember.id,
      order_status_id: order_status.id,
    },
    include: ["order_details", "order_status"],
  });

  res.status(200).json(orders);
};

export const my_order_detail = async (req: Request, res: Response) => {
  const { currentMember } = req;
  const { id } = req.query;

  if (!id || !currentMember) {
    throw new BadRequestError("Request is invalid!");
  }

  if (typeof id !== "string") {
    throw new BadRequestError("Request is invalid!");
  }

  const order = await Order.findOne({
    where: {
      member_id: currentMember.id,
      id: id,
    },
    include: [
      "order_details",
      "order_status",
      {
        association: "order_status_histories",
        include: ["order_status"],
      },
    ],
  });

  res.status(200).json(order);
};

export const check_referral_code = async (req: Request, res: Response) => {
  const { referral_code } = req.query;

  if (typeof referral_code !== "string") {
    throw new BadRequestError("Referral code is not valid!");
  }

  const member = await Member.findOne({ where: { referral_code } });

  if (!member) {
    throw new BadRequestError("Referral Code is invalid!");
  }

  res.status(200).json(member);
};

export const redeem_point = async (req: Request, res: Response) => {
  const current_member = req.currentMember;

  const { product_id, shipping_address_id } = req.body;

  const product = await Product.findByPk(product_id);
  const shipping_address = await MemberShippingAddress.findByPk(
    shipping_address_id
  );

  if (!product || !shipping_address || !current_member) {
    throw new BadRequestError(
      "Current User / Product / Shipping Address is invalid!"
    );
  }

  const point_ledgers = await MemberPointLedger.findAll({
    where: { member_id: current_member.id },
  });

  const total_point = point_ledgers
    .map((ledger) => ledger.amount)
    .reduce(
      (total_current_point, current_point) =>
        total_current_point + current_point,
      0
    );

  if (total_point < product.price) {
    throw new BadRequestError("Point is insufficient");
  }

  const order_status_default = await OrderStatus.findOne({
    where: { code: "menunggu_pembayaran" },
  });
  const order_status_processing = await OrderStatus.findOne({
    where: { code: "processing" },
  });

  if (!order_status_processing || !order_status_default) {
    throw new BadRequestError("Order Status is invalid!");
  }

  const shipping_service = await checkServicePrice(
    shipping_address,
    [{ product, quantity: 1 }],
    "sicepat",
    "REG"
  );

  const order = await Order.create({
    member_id: current_member.id,
    contact_address: shipping_address.contact_address,
    contact_name: shipping_address.contact_name,
    contact_phone: shipping_address.contact_phone,
    contact_email: current_member.email,
    order_status_id: order_status_default.id,
    province_code: shipping_address.province_code,
    province_name: shipping_address.province_name,
    city_code: shipping_address.city_code,
    city_name: shipping_address.city_name,
    subdistrict_code: shipping_address.subdistrict_code,
    subdistrict_name: shipping_address.subdistrict_name,
    postal_code: shipping_address.postal_code,
    address_note: shipping_address.note,
    shipping_courier_code: shipping_service.courierCode,
    shipping_courier_service: shipping_service.service,
    shipping_price: shipping_service.price,
    shipping_type: "courier",
  });

  await OrderDetail.create({
    order_id: order.id,
    product_id: product.id,
    name: product.name,
    description: product.description,
    quantity: 1,
    thumbnail: product.thumbnail,
    weight: product.weight,
    height: product.height,
    length: product.length,
    width: product.width,
    price: product.price,
    final_weight: product.weight,
    final_price: product.price,
  });

  order.set("order_status_id", order_status_processing.id);
  await order.save();

  res.status(200).send({ message: "Redeem Success" });
};

export const my_address = async (req: Request, res: Response) => {
  const current_member = req.currentMember;

  if (!current_member) {
    throw new BadRequestError("Invalid Request!");
  }

  const addresses = await MemberShippingAddress.findAll({
    where: { member_id: current_member.id },
  });

  res.status(200).json(addresses);
};

export const make_order = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember) {
    throw new NotAuthorizedError();
  }

  const {
    items,
    shipping_type,
    member_shipping_address_id,
    shipping_courier_code,
    shipping_courier_service,
  } = req.body;

  const cart_products: { product: Product; quantity: number }[] = [];

  for (const item of items) {
    const product = await Product.findByPk(item.product_id, {
      include: ["product_level_prices"],
    });

    if (!product) {
      continue;
    }

    cart_products.push({ product, quantity: item.quantity });
  }

  if (cart_products.length === 0) {
    throw new BadRequestError("Cart is empty!");
  }

  const shipping_address = await MemberShippingAddress.findByPk(
    member_shipping_address_id
  );

  if (!shipping_address) {
    throw new BadRequestError("Shipping Address is invalid!");
  }

  const order_status = await OrderStatus.findOne({
    where: { code: "menunggu_pembayaran" },
  });

  if (!order_status) {
    throw new Error("Order Status is invalid!");
  }

  const shipping_data: {
    shipping_courier_code: string | undefined;
    shipping_courier_service: string | undefined;
    shipping_price: number;
  } = {
    shipping_courier_code: undefined,
    shipping_courier_service: undefined,
    shipping_price: 0,
  };

  if (shipping_type !== "pickup") {
    const shipping_service = await checkServicePrice(
      shipping_address,
      cart_products,
      shipping_courier_code,
      shipping_courier_service
    );

    shipping_data.shipping_courier_code = shipping_service.courierCode;
    shipping_data.shipping_courier_service = shipping_service.service;
    shipping_data.shipping_price = shipping_service.price;
  }

  // const shipping_service = await checkServicePrice(
  //   shipping_address,
  //   cart_products,
  //   shipping_courier_code,
  //   shipping_courier_service
  // );

  const order = await Order.create({
    member_id: currentMember.id,
    contact_address: shipping_address.contact_address,
    contact_name: shipping_address.contact_name,
    contact_phone: shipping_address.contact_phone,
    contact_email: shipping_address.contact_email,
    order_status_id: order_status.id,
    province_code: shipping_address.province_code,
    province_name: shipping_address.province_name,
    city_code: shipping_address.city_code,
    city_name: shipping_address.city_name,
    subdistrict_code: shipping_address.subdistrict_code,
    subdistrict_name: shipping_address.subdistrict_name,
    postal_code: shipping_address.postal_code,
    address_note: shipping_address.note,
    shipping_type,
    ...shipping_data,
  });

  const order_detail_payloads: OrderDetailCreationAttributes[] = [];
  cart_products.forEach((cp) => {
    order_detail_payloads.push({
      order_id: order.id,
      product_id: cp.product.id,
      name: cp.product.name,
      description: cp.product.description,
      quantity: cp.quantity,
      thumbnail: cp.product.thumbnail,
      weight: cp.product.weight,
      height: cp.product.height,
      length: cp.product.length,
      width: cp.product.width,
      price: cp.product.price,
      final_weight: cp.product.weight,
      // @ts-ignore
      final_price: cp.product.product_level_prices?.find(
        (plp: any) => plp.member_level_id === currentMember.member_level_id
      )?.price,
    });
  });

  const order_details = await OrderDetail.bulkCreate(order_detail_payloads);

  let midtrans_price = 0;

  midtrans_price += order.shipping_price;
  order_details.forEach((order_detail) => {
    midtrans_price += order_detail.final_price * order_detail.quantity;
  });

  const response_snap = await snapClient.post(`/snap/v1/transactions`, {
    transaction_details: {
      order_id: order.id,
      gross_amount: midtrans_price,
    },
    customer_details: {
      first_name: currentMember.name,
      email: currentMember.email,
      phone: currentMember.phone,
    },
    expiry: {
      unit: "hours",
      duration: 1,
    },
  });

  order.set("payment_url", response_snap.data.redirect_url);
  await order.save();

  if (typeof process.env.NODEMAILER_USER !== "string") {
    throw new Error("NODEMAILER_USER must be defined.");
  }
  if (typeof process.env.NODEMAILER_PASS !== "string") {
    throw new Error("NODEMAILER_PASS must be defined.");
  }

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS,
    },
  });

  const mailOptions = {
    from: process.env.NODEMAILER_USER,
    to: "jankosglowindonesia@gmail.com",
    subject: `Pesanan Baru - ${currentMember.name}`,
    html: `
    <p>Telah masuk order baru oleh user ${currentMember.name}.</p>
    <p>Detail Pesanan Dapat dilihat pada link berikut.</p>
    <br/>
    <a href="${process.env.APP_CLIENT_HOST}/cms/order/${order.id}">Click Here</a>
    <br/>
    <p>Atau salin link berikut</p>
    <p>${process.env.APP_CLIENT_HOST}/cms/order/${order.id}</p>
    `,
  } as MailOptions;

  await transporter.sendMail(mailOptions);

  res.status(200).json({ message: "Order Created!" });
};

export const commistion_payout = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember) {
    throw new NotAuthorizedError();
  }

  const member_referrals = await MemberReferral.findAll({
    where: { upline_member_id: currentMember.id },
    include: "member_referral_ledgers",
  });

  const current_balance = member_referrals
    .map((referral) =>
      // @ts-ignore
      referral.member_referral_ledgers
        .map((ledger) => ledger.amount)
        .reduce(
          (total_price, current_ledger) => total_price + current_ledger,
          0
        )
    )
    .reduce((total_price, current_price) => total_price + current_price, 0);

  const { amount } = req.body;

  if (amount > current_balance) {
    throw new BadRequestError("Insufficient Balance");
  }

  const new_referral_ledger = await MemberReferralLedger.create({
    amount,
    member_referral_id: member_referrals[0].id,
  });

  res.status(200).json(new_referral_ledger);
};

export const update_my_account = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember) {
    throw new NotAuthorizedError();
  }

  const {
    name,
    phone,
    email,
    bank_name,
    bank_account_name,
    bank_account_number,
    address,
  } = req.body;

  let member_image_id_card = null;
  const { image_id_card } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };
  if (image_id_card && image_id_card[0]) {
    member_image_id_card = image_id_card[0].path;
  }

  currentMember.set("name", name);
  currentMember.set("phone", phone);
  currentMember.set("email", email);
  currentMember.set("address", address);
  if (member_image_id_card) {
    currentMember.set("image_id_card", member_image_id_card);
  }

  await currentMember.save();

  await MemberBankAccount.update(
    { bank_name, bank_account_name, bank_account_number },
    { where: { member_id: currentMember.id } }
  );

  return res.status(200).json(currentMember);
};

export const shipping_label = async (req: Request, res: Response) => {
  // const { currentAdmin } = req;

  // if (!currentAdmin) {
  //   throw new NotAuthorizedError();
  // }

  const { order_id } = req.params;

  const order = await Order.findByPk(order_id);

  if (!order) {
    throw new NotFoundError();
  }

  const shipdeo_client = await shipdeoClient();

  const { data } = await shipdeo_client.post(
    "/v1/shipping-label/plugins",
    {
      display_type: "pdf",
      return_link_only: false,
      ordersId: [order.shipping_id],
      additional_info: {
        printThermal: true,
      },
      showField: {
        orderDetail: true,
        senderAddress: true,
        tenantLogo: true,
        showBarcodeNoOrder: true,
        showBarcodeTrackingNumber: true,
        showCreatedDateRelated: true,
        showShippingCharge: true,
      },
      config: {
        template: {
          marginsType: 0,
          landscape: false,
          printBackground: false,
          fitToPage: true,
          width: "80mm",
          height: "100mm",
          marginTop: "15px",
          marginRight: "15px",
          marginBottom: "15px",
          marginLeft: "15px",
        },
      },
    },
    {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/pdf",
      },
    }
  );

  res
    .status(200)
    .set("Content-Type", `application/json; name="${order.id}.pdf"`)
    .set("Content-Disposition", `attachment; filename="${order.id}.pdf"`)
    .send(data);
};

export const pickup_order = async (req: Request, res: Response) => {
  process.env.TZ = "Asia/Jakarta";
  const { currentAdmin } = req;

  if (!currentAdmin) {
    throw new NotAuthorizedError();
  }

  const { order_id } = req.params;

  const order = await Order.findByPk(order_id, { include: ["order_status"] });

  if (!order || !order.order_status) {
    throw new NotFoundError();
  }

  if (order.order_status.code !== "processing") {
    throw new BadRequestError("Order is not ready for pickup!");
  }

  const shipdeo_client = await shipdeoClient();

  console.log(format(addHours(new Date(), 1), "MM/dd/yyyy hh:mm:ss aa xx"));

  const response = await shipdeo_client.patch(
    `/v1/couriers/orders/${order.shipping_id}`,
    {
      delivery_type: "pickup",
      delivery_time: format(
        addHours(new Date(), 1),
        "MM/dd/yyyy hh:mm:ss aa xx"
      ),
    }
  );

  if (response?.data?.data?.tracking_info?.airwaybill) {
    order.set("airwaybill_number", response.data.data.tracking_info.airwaybill);
  }

  const order_status = await OrderStatus.findOne({
    where: { code: "request_pickup" },
  });

  if (!order_status) {
    throw new BadRequestError("Order status for request pickup is not exist!");
  }

  order.set("order_status_id", order_status.id);
  await order.save();

  res.status(200).json({ message: "order shipped!" });
};

export const create_withdrawal = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember || !currentMember.member_bank_account) {
    throw new NotAuthorizedError();
  }

  const { amount } = req.body;

  if (!amount) {
    throw new BadRequestError("Amount is invalid!");
  }

  const withdrawal_ledger = await MemberWithdrawalLedger.create({
    amount,
    member_id: currentMember.id,
    status: "Request Approval",
  });

  res.status(200).json(withdrawal_ledger);
};

export const approve_withdrawal = async (req: Request, res: Response) => {
  const { member_withdrawal_ledger_id } = req.body;

  const member_withdrawal_ledger = await MemberWithdrawalLedger.findByPk(
    member_withdrawal_ledger_id,
    {
      include: [
        {
          association: "member",
          include: ["member_bank_account"],
        },
      ],
    }
  );

  if (
    !member_withdrawal_ledger ||
    !member_withdrawal_ledger.member ||
    !member_withdrawal_ledger.member.member_bank_account
  ) {
    throw new BadRequestError("Member Withdrawal ledger not Found!");
  }

  // CHECK EXISTING IRIS BENEFICIARY ACCOUNT
  const { data: beneficiaries } = await irisCreatorClient.get(
    "/api/v1/beneficiaries"
  );

  let found_account = beneficiaries.find(
    (beneficiary: any) =>
      // @ts-ignore
      beneficiary.email === member_withdrawal_ledger.member.email
  );

  // IF NOT EXIST, CREATE NEW BENEFICIARY ACCOUNT
  if (!found_account) {
    found_account = {
      name: member_withdrawal_ledger.member.name,
      account:
        member_withdrawal_ledger.member.member_bank_account.bank_account_number,
      bank: member_withdrawal_ledger.member.member_bank_account.bank_name.toLocaleLowerCase(),
      alias_name:
        member_withdrawal_ledger.member.member_bank_account.bank_account_name
          .toLocaleLowerCase()
          .trim(),
      email: member_withdrawal_ledger.member.email,
    };

    await irisCreatorClient.post("/api/v1/beneficiaries", found_account);
  }

  // CREATE PAYOUT
  const { data: payout_response } = await irisCreatorClient.post(
    "/api/v1/payouts",
    {
      beneficiary_name: found_account.name,
      beneficiary_account: found_account.account,
      beneficiary_bank: found_account.bank,
      beneficiary_email: found_account.email,
      amount: member_withdrawal_ledger.amount - 5000,
      notes: `Withdrawal Jankos Glow Account ${Date.now()}`,
    }
  );

  const { status, reference_no } = payout_response;

  member_withdrawal_ledger.set("reference_no", reference_no);
  member_withdrawal_ledger.set("status", status);
  await member_withdrawal_ledger.save();

  res.status(200).json({ message: "Withdrawal Approved!" });
};

export const iris_notification = async (req: Request, res: Response) => {
  const { reference_no, amount, status } = req.body;

  const withdrawal_ledger = await MemberWithdrawalLedger.findOne({
    where: {
      reference_no,
    },
  });

  if (withdrawal_ledger && withdrawal_ledger.status !== status) {
    withdrawal_ledger.set("status", status);
    await withdrawal_ledger.save();
  }

  res.status(200).json({ message: "success!" });
};

export const shipdeo_notification = async (req: Request, res: Response) => {
  const {
    courierChannelId,
    airwayBillNumber,
    status,
    statusDateTime,
    lastStatus,
    lastStatusDateTime,
    receiverName,
    receiverRelation,
    note,
  } = req.body;

  const order = await Order.findOne({
    where: {
      airwaybill_number: airwayBillNumber,
    },
    include: ["order_status"],
  });

  if (!order || !order.order_status) {
    throw new BadRequestError("Airwaybill Number is not registered!");
  }

  const order_status = await OrderStatus.findOne({ where: { code: status } });

  if (!order_status) {
    throw new BadRequestError("Status is not registered!");
  }

  if (order.order_status.code !== order_status.code) {
    order.set("order_status_id", order_status.id);
    await order.save();

    await OrderStatusHistory.update(
      {
        note: `${note}${receiverName && `| [Penerima:${receiverName}]`}`,
      },
      {
        where: {
          order_id: order.id,
          order_status_id: order_status.id,
        },
      }
    );
  }
};

export const add_shipping_address = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember || !currentMember.member_bank_account) {
    throw new NotAuthorizedError();
  }

  const {
    city_code,
    city_name,
    contact_address,
    postal_code,
    province_code,
    province_name,
    subdistrict_code,
    subdistrict_name,
  } = req.body;

  const member_shipping_address = await MemberShippingAddress.create({
    address_name: "Shipping Address",
    city_code,
    city_name,
    contact_address,
    contact_email: currentMember.email,
    contact_name: currentMember.name,
    contact_phone: currentMember.phone,
    member_id: currentMember.id,
    postal_code,
    province_code,
    province_name,
    subdistrict_code,
    subdistrict_name,
  });

  res.status(200).json(member_shipping_address);
};

export const forget_password = async (req: Request, res: Response) => {
  const { email } = req.body;

  const member = await Member.findOne({
    where: { email },
  });

  if (!member) {
    throw new BadRequestError("Email is not registered!");
  }

  if (typeof process.env.APP_JWT_KEY !== "string") {
    throw new Error("APP_JWT_KEY must be defined.");
  }

  const change_password_token = jwt.sign(
    { member_id: member.id },
    process.env.APP_JWT_KEY,
    {
      expiresIn: "5 minutes",
    }
  );

  if (typeof process.env.NODEMAILER_USER !== "string") {
    throw new Error("NODEMAILER_USER must be defined.");
  }
  if (typeof process.env.NODEMAILER_PASS !== "string") {
    throw new Error("NODEMAILER_PASS must be defined.");
  }

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS,
    },
  });

  const mailOptions = {
    from: process.env.NODEMAILER_USER,
    to: member.email,
    subject: "Reset Password Request",
    html: `
    <p>Here is the link to reset your password.</p>
    <p>This link is only valid for 5 minutes.</p>
    <br/>
    <a href="${process.env.APP_CLIENT_HOST}/auth/change-password?change_password_token=${change_password_token}">Click Here</a>
    <br/>
    <p>Or Copy This Link</p>
    <p>${process.env.APP_CLIENT_HOST}/auth/change-password?change_password_token=${change_password_token}</p>
    `,
  } as MailOptions;

  await transporter.sendMail(mailOptions);

  res.status(200).json({ change_password_token });
};

export const change_password = async (req: Request, res: Response) => {
  const { new_password, change_password_token } = req.body;

  if (typeof process.env.APP_JWT_KEY !== "string") {
    throw new Error("APP_JWT_KEY must be defined.");
  }

  const payload = jwt.verify(
    change_password_token,
    process.env.APP_JWT_KEY
  ) as { member_id: string };

  const member = await Member.findByPk(payload.member_id);

  if (!member) {
    throw new BadRequestError("Change Password Token is invalid!");
  }

  member.set("password", new_password);
  await member.save();

  res.status(200).json(member);
};

export const send_email_support = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember || !currentMember.member_bank_account) {
    throw new NotAuthorizedError();
  }

  const { subject, body } = req.body;

  if (typeof process.env.NODEMAILER_USER !== "string") {
    throw new Error("NODEMAILER_USER must be defined.");
  }
  if (typeof process.env.NODEMAILER_PASS !== "string") {
    throw new Error("NODEMAILER_PASS must be defined.");
  }

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS,
    },
  });

  const mailOptions = {
    from: process.env.NODEMAILER_USER,
    to: process.env.NODEMAILER_USER,
    subject: `Email Bantuan`,
    html: `
    <p>User: ${currentMember.name}</p>
    <p>Email: ${currentMember.email}</p>
    <p>Subjek: ${subject}</p>
    <p>Pesan: ${body}</p>
    `,
  } as MailOptions;

  await transporter.sendMail(mailOptions);

  res.status(200).json({ message: "Success send support email!" });
};

export const my_transaction_monthly = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember || !currentMember.member_bank_account) {
    throw new NotAuthorizedError();
  }

  const transaction_montly: number[] = [];

  let year = new Date().getFullYear();

  if (typeof req.query.year == "string") {
    year = parseInt(req.query.year);
  }

  const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const orders = await Order.findAll({
    attributes: ["createdAt"],
    where: {
      member_id: currentMember.id,
      createdAt: {
        [Op.gte]: `${year}-01-01`,
        [Op.lte]: `${year}-12-31`,
      },
    },
  });
  months.forEach((m) => {
    let total = 0;
    orders.forEach((o) => {
      if (
        o.createdAt.getFullYear() == year &&
        o.createdAt.getMonth() + 1 == m
      ) {
        total = total + 1;
      }
    });
    transaction_montly.push(total);
  });

  res.status(200).json(transaction_montly);
};

export const download_invoice = async (req: Request, res: Response) => {
  let { order_id } = req.query;

  if (typeof order_id !== "string") {
    throw new BadRequestError("Order ID is Invalid!");
  }

  const order = await Order.findOne({
    where: { id: order_id },
    include: ["order_details", "order_status"],
  });

  if (!order) {
    throw new BadRequestError("Order ID is Invalid!");
  }

  const content = generate_user_invoice_email(order, true);

  res.render("all", { content, layout: false });
};
