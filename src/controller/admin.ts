import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { tokenize } from "../helpers/function/jwt";
import { PasswordClient } from "../helpers/function/password-client";
import { Admin } from "../models/admin";
import { Op } from "sequelize";

export const create = async (req: Request, res: Response) => {
  const { name, email, password, admin_level_id } = req.body;

  const existing_admin = await Admin.findOne({ where: { email } });
  if (existing_admin) {
    throw new BadRequestError("Email is already registered!");
  }

  const admin = await Admin.create({
    name,
    email,
    password,
    admin_level_id,
  });

  res.status(200).json(admin);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_admin = await Admin.findByPk(id, { include: ["admin_level"] });
  if (!existing_admin || !existing_admin.admin_level) {
    throw new NotFoundError();
  }

  // Check if existing user is superadmin
  if (existing_admin.admin_level.hierarchy_number === 999) {
    // Check if updated level is not superadmin
    if (req.body.admin_level_id !== existing_admin.admin_level_id) {
      const other_superadmin = await Admin.findAll({
        where: {
          id: {
            [Op.not]: existing_admin.id,
          },
          admin_level_id: existing_admin.admin_level_id,
        },
      });
      // Check for last superadmin prevention level change
      if (other_superadmin.length === 0) {
        throw new BadRequestError("Cannot change level of last Super Admin!");
      }
    }
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_admin.set(key, req.body[key]);
    }
  }

  await existing_admin.save();

  res.status(200).json(existing_admin);
};

export const delete_admin = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_admin = await Admin.findByPk(id);

  if (!existing_admin || !existing_admin.admin_level) {
    throw new NotFoundError();
  }

  // Check if existing user is superadmin
  if (existing_admin.admin_level.hierarchy_number === 999) {
    const other_superadmin = await Admin.findAll({
      where: {
        id: {
          [Op.not]: existing_admin.id,
        },
        admin_level_id: existing_admin.admin_level_id,
      },
    });
    // Check for last superadmin prevention level change
    if (other_superadmin.length === 0) {
      throw new BadRequestError("Cannot delete last Super Admin!");
    }
  }

  existing_admin.destroy();

  res.status(200).json({ message: "Success deleted!" });
};

export const signin = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const existing_admin = await Admin.findOne({ where: { email: email } });

  if (!existing_admin) {
    throw new BadRequestError("Invalid credentials.");
  }

  const passwordMatch = await PasswordClient.compare(
    existing_admin.password,
    password
  );

  if (!passwordMatch) {
    throw new BadRequestError("Invalid credentials.");
  }

  // Generate token
  const token = tokenize({
    id: existing_admin.id,
    email: existing_admin.email,
  });

  // Assign token to session
  res.status(200).send({ jwt: token });
};

export const currentAdmin = (req: Request, res: Response) => {
  // @ts-ignore
  res.send({ current_admin: req.currentAdmin || null });
};
