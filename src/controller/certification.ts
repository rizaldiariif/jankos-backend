import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { Certification } from "../models/certification";

export const create = async (req: Request, res: Response) => {
  let certification_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    certification_image_thumbnail = image_thumbnail[0].path;
  }

  if (!certification_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const certification = await Certification.create({
    thumbnail: certification_image_thumbnail,
  });

  res.status(200).json(certification);
};

export const delete_certification = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_certification = await Certification.findByPk(id);

  if (!existing_certification) {
    throw new NotFoundError();
  }

  existing_certification.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
