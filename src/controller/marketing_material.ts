import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { MarketingMaterial } from "../models/marketing_material";

export const create = async (req: Request, res: Response) => {
  const { title, text, suggestion_date, code } = req.body;

  let marketing_material_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    marketing_material_image_thumbnail = image_thumbnail[0].path;
  }

  if (!marketing_material_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const product = await MarketingMaterial.create({
    title,
    text,
    suggestion_date,
    code,
    thumbnail: marketing_material_image_thumbnail,
  });

  res.status(200).json(product);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_marketing_material = await MarketingMaterial.findByPk(id);
  if (!existing_marketing_material) {
    throw new NotFoundError();
  }

  let marketing_material_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    marketing_material_image_thumbnail = image_thumbnail[0].path;
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_marketing_material.set(key, req.body[key]);
    }
  }

  if (marketing_material_image_thumbnail) {
    existing_marketing_material.set(
      "thumbnail",
      marketing_material_image_thumbnail
    );
  }
  await existing_marketing_material.save();

  res.status(200).json(existing_marketing_material);
};

export const delete_marketing_material = async (
  req: Request,
  res: Response
) => {
  const { id } = req.params;

  const existing_marketing_material = await MarketingMaterial.findByPk(id);

  if (!existing_marketing_material) {
    throw new NotFoundError();
  }

  existing_marketing_material.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
