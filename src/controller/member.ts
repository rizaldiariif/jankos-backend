import { Request, Response } from "express";
import changePassword from "../emails/member/changePassword";
import { BadRequestError } from "../errors/bad-request-error";
import { NotAuthorizedError } from "../errors/not-authorized-error";
import { NotFoundError } from "../errors/not-found-error";
import { sendEmail } from "../helpers/api/mailgun";
import { tokenize, tokenizeVerification } from "../helpers/function/jwt";
import { PasswordClient } from "../helpers/function/password-client";
import { Member } from "../models/member";
import { MemberLevel } from "../models/member_level";
import jwt from "jsonwebtoken";

export const detail = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_member = await Member.findByPk(id, {
    include: [
      "member_shipping_addresses",
      {
        association: "member_level_histories",
        include: ["member_level"],
      },
      "member_bank_account",
      {
        association: "member_referrals",
        include: ["upline_member", "referred_member"],
      },
      {
        association: "orders",
        include: ["order_status"],
      },
      "member_level",
      "member_point_ledgers",
    ],
  });

  if (!existing_member) {
    throw new NotFoundError();
  }

  res.status(200).json(existing_member);
};

export const signup = async (req: Request, res: Response) => {
  const {
    name,
    email,
    phone,
    password,
    address,
    social_media_url,
    marketplace_url,
  } = req.body;

  let // member_image_profile,
    member_image_id_card = null;

  const existing_member = await Member.findOne({ where: { email } });
  if (existing_member) {
    throw new BadRequestError("Email is already registered");
  }

  const member_level = await MemberLevel.findOne({
    where: { hierarchy_number: 1 },
  });

  if (!member_level) {
    throw new BadRequestError("There is no default member level!");
  }

  const {
    // image_profile,
    image_id_card,
  } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  // if (image_profile && image_profile[0]) {
  //   member_image_profile = image_profile[0].path;
  // }

  if (image_id_card && image_id_card[0]) {
    member_image_id_card = image_id_card[0].path;
  }

  if (
    // !member_image_profile ||
    !member_image_id_card
  ) {
    throw new BadRequestError("Image File is not completed!");
  }

  const member = await Member.create({
    name,
    email,
    phone,
    password,
    address,
    member_level_id: member_level.id,
    // image_profile: member_image_profile,
    image_id_card: member_image_id_card,
    social_media_url,
    marketplace_url,
  });

  // Generate Token
  const token = tokenize({ id: member.id, email: member.email });

  res.status(200).json({ jwt: token });
};

export const signin = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const existing_member = await Member.findOne({ where: { email: email } });

  if (!existing_member) {
    throw new BadRequestError("Invalid credentials.");
  }

  const passwordMatch = await PasswordClient.compare(
    existing_member.password,
    password
  );

  if (!passwordMatch) {
    throw new BadRequestError("Invalid credentials.");
  }

  // Generate token
  const token = tokenize({
    id: existing_member.id,
    email: existing_member.email,
  });

  // Assign token to session
  res.status(200).send({ jwt: token });
};

export const currentMember = (req: Request, res: Response) => {
  // @ts-ignore
  res.send({ current_member: req.currentMember || null });
};

export const update_my_profile = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember) {
    throw new NotAuthorizedError();
  }

  const {
    name,
    phone,
    //  email,
    address,
  } = req.body;

  currentMember.set("name", name);
  currentMember.set("phone", phone);
  // currentMember.set("email", email);
  currentMember.set("address", address);

  await currentMember.save();

  res.status(200).json(currentMember);
};

export const update_my_password = async (req: Request, res: Response) => {
  const { currentMember } = req;

  if (!currentMember) {
    throw new NotAuthorizedError();
  }

  const { current_password, password } = req.body;

  const passwordMatch = await PasswordClient.compare(
    currentMember.password,
    current_password
  );

  if (!passwordMatch) {
    throw new BadRequestError("Current password is incorrect!");
  }

  currentMember.set("password", password);
  await currentMember.save();

  res.status(200).json(currentMember);
};

export const request_forget_password = async (req: Request, res: Response) => {
  const { email } = req.body;

  const member = await Member.findOne({ where: { email } });

  if (!member) {
    throw new NotFoundError();
  }

  const verification_token = tokenizeVerification({
    id: member.id,
  });

  await sendEmail(
    email,
    "Request of Reset Password",
    changePassword(verification_token)
  );

  res.status(200).json({ message: "success" });
};

export const process_forget_password = async (req: Request, res: Response) => {
  const { verification_token, password } = req.body;

  const { id } = jwt.verify(verification_token, process.env.APP_JWT_KEY!) as {
    id: string;
  };

  const member = await Member.findByPk(id);

  if (!member) {
    throw new NotAuthorizedError();
  }

  member.set("password", password);
  await member.save();

  res.status(200).json(member);
};
