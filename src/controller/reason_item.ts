import { Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { NotFoundError } from "../errors/not-found-error";
import { ReasonItem } from "../models/reason_item";

export const create = async (req: Request, res: Response) => {
  const { text, title } = req.body;

  let reason_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    reason_image_thumbnail = image_thumbnail[0].path;
  }

  if (!reason_image_thumbnail) {
    throw new BadRequestError("Image File is not completed!");
  }

  const testimony = await ReasonItem.create({
    title,
    text,
    thumbnail: reason_image_thumbnail,
  });

  res.status(200).json(testimony);
};

export const update = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_reason = await ReasonItem.findByPk(id);
  if (!existing_reason) {
    throw new NotFoundError();
  }

  let reason_image_thumbnail = null;
  const { image_thumbnail } = req.files as {
    [fieldname: string]: Express.Multer.File[];
  };

  if (image_thumbnail && image_thumbnail[0]) {
    reason_image_thumbnail = image_thumbnail[0].path;
  }

  for (const key of Object.keys(req.body)) {
    if (req.body[key]) {
      // @ts-ignore
      existing_reason.set(key, req.body[key]);
    }
  }

  if (reason_image_thumbnail) {
    existing_reason.set("thumbnail", reason_image_thumbnail);
  }
  await existing_reason.save();

  res.status(200).json(existing_reason);
};

export const delete_reason = async (req: Request, res: Response) => {
  const { id } = req.params;

  const existing_reason = await ReasonItem.findByPk(id);

  if (!existing_reason) {
    throw new NotFoundError();
  }

  existing_reason.destroy();

  res.status(200).json({ message: "Success deleted!" });
};
