import axios from "axios";
import { Request, Response } from "express";
import { MemberLevel } from "../models/member_level";
import { Product } from "../models/product";
import FormData from "form-data";
import path from "path";
import fs from "fs";
import { Member } from "../models/member";
import { BadRequestError } from "../errors/bad-request-error";
import { db } from "../config/db";
import { MarketingMaterial } from "../models/marketing_material";
import { OrderStatus } from "../models/order_status";
import { AdminLevel } from "../models/admin_level";
import { Admin } from "../models/admin";
import articles from "../demo-data/articles";
import { Article } from "../models/article";
import { Testimony } from "../models/testimony";
import page_contents from "../demo-data/page_contents";
import { PageContent } from "../models/page_content";
import { ProductCategory } from "../models/product_category";
import reason_items from "../demo-data/reason_items";
import { ReasonItem } from "../models/reason_item";
import { ProductImage } from "../models/product_image";
import { BannerHome } from "../models/banner_home";
import { Certification } from "../models/certification";

export const signup_flow = async (req: Request, res: Response) => {
  try {
    const product = await Product.findOne();
    const member_level = await MemberLevel.findOne();

    if (!member_level) {
      throw new BadRequestError("There is no default member level!");
    }
    if (!product) {
      throw new BadRequestError("There is no default product!");
    }

    const demo_client = axios.create({ baseURL: process.env.APP_HOST });

    const dummy_member = await Member.findOne({
      where: { email: "jankosglowindonesia@gmail.com" },
    });

    const signup_data: any = {
      name: `User ${Math.ceil(Math.random() * 100)}`,
      email: `user${Math.ceil(Math.random() * 100)}@gmail.com`,
      phone: `${Math.ceil(Math.random() * 100000000000)}`,
      password: "ABCabc123_",
      bank_name: "BCA",
      bank_account_name: "Dummy User",
      bank_account_number: `${Math.ceil(Math.random() * 100000000000)}`,
      address: "Jl. Ratu Teratai No.35, RT.4/RW.13, Duri Kepa",
      member_level_id: member_level.id,
      product_id: product.id,
      quantity: 500,
      shipping_address: "Jl. Ratu Teratai No.35, RT.4/RW.13, Duri Kepa",
      shipping_subdistrict_code: "31.73.05",
      shipping_postal_code: "11520",
      shipping_courier_code: "sicepat",
      shipping_courier_service: "REG",
      shipping_type: "courier",
      social_media_url: "https://instagram.com",
      marketplace_url: "https://tokopedia.com",
      products: JSON.stringify([
        {
          product_id: product.id,
          quantity: 500,
        },
      ]),
    };

    if (dummy_member) {
      signup_data.referral_code = dummy_member.referral_code;
    } else {
      signup_data.name = "Dummy User";
      signup_data.email = "jankosglowindonesia@gmail.com";
      signup_data.password = "ABCabc123_";
    }

    const form = new FormData();

    for (const key of Object.keys(signup_data)) {
      form.append(key, signup_data[key]);
    }
    form.append(
      "image_id_card",
      fs.createReadStream(
        path.resolve(__dirname, "../../demo_files/demo-image.png")
      )
    );

    const response_1 = await demo_client.post("/api/flow/signup", form, {
      headers: {
        ...form.getHeaders(),
      },
    });

    res.json({ message: "success", response: response_1.data });
  } catch (error: any) {
    console.log(error);
    res.json(error);
  }
};

export const current_member_flow = async (req: Request, res: Response) => {
  try {
    const demo_client = axios.create({ baseURL: process.env.APP_HOST });

    const member = await Member.findOne();

    if (!member) {
      throw new BadRequestError("There is no registered user!");
    }

    const {
      data: { jwt },
    } = await demo_client.post("/api/members/auth/signin", {
      email: member.email,
      password: "ABCabc123_",
    });

    const {
      data: { current_member },
    } = await demo_client.get("/api/members/auth/me", {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });

    res.status(200).json(current_member);
  } catch (error: any) {
    console.log(error.response.data);
  }
};

export const sync = async (req: Request, res: Response) => {
  await db.sync();

  await BannerHome.bulkCreate([
    {
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1664732002/jankos/Rectangle_62_q3xn6z.png",
    },
    {
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1664732002/jankos/Rectangle_62_q3xn6z.png",
    },
  ]);

  await Certification.bulkCreate([
    {
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1664731602/jankos/Group_506_3_cvnwjf.png",
    },
    {
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1664731602/jankos/Group_505_1_i2cj2s.png",
    },
    {
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1664731655/jankos/Group_552_1_ukp7nv.png",
    },
  ]);

  await MemberLevel.bulkCreate([
    {
      name: `Bronze`,
      commision_percentage: 5,
      hierarchy_number: 1,
      minimum_order: 12,
      price_cut_percentage: 3,
    },
    {
      name: `Silver`,
      commision_percentage: 10,
      hierarchy_number: 2,
      minimum_order: 60,
      price_cut_percentage: 6,
    },
    {
      name: `Gold`,
      commision_percentage: 15,
      hierarchy_number: 3,
      minimum_order: 120,
      price_cut_percentage: 9,
    },
    {
      name: `Diamond`,
      commision_percentage: 20,
      hierarchy_number: 4,
      minimum_order: 1000,
      price_cut_percentage: 12,
    },
  ]);

  const [whitening_series, acne_series, sparkling_glow_foam] =
    await ProductCategory.bulkCreate([
      {
        name: "Whitening Series",
        slug: "whitening-series",
      },
      {
        name: "Acne Series",
        slug: "acne-series",
      },
      {
        name: "Sparkling Glow Foam",
        slug: "sparkling-glow-foam",
      },
    ]);

  const super_glow_serum = await Product.create({
    product_category_id: whitening_series.id,
    name: "Super Glow Serum",
    is_starter: false,
    description: `
      <p>Super glow serum yang terdiri dari kombinasi 7 ekstrak bunga, Hydrolyzed Hyaluronic Acid, dan Pro Vitamin B5 menjaga kelembapan serta memberikan nutrisi untuk merawat kulit kusam. Dipadukan dengan Niacinamide, Vitamin E dan Aloe Vera, membantu mencerahkan dan meratakan warna kulit yang tidak rata.</p>
      <br/>
      <p><b>Kandungan</b></p>
      <p>7 ekstrak bunga, Hydrolyzed Hyaluronic Acid, Pro Vitamin B5, Niacinamide, Vitamin E dan Aloe Barbadensis Leaf Extract.</p>
      <br/>
      <p><b>Manfaat :</b></p>
      <ul>
        <li>Menjadikan kulit cerah dan glowing</li>
        <li>Meratakan warna kulit</li>
        <li>Memberikan nutrisi dan menjaga kelembapan kulit</li>
        <li>Merawat kulit yang kusam</li>
        <li>Meregenerasi kulit</li>
      </ul>
      <br/>
      <p><b>Cara Pemakaian</b></p>
      <p>Tuangkan pada telapak tangan secukupnya, aplikasikan ke wajah dan pijat secara lembut.</p>
      <br/>
      <p><b>Cocok untuk</b></p>
      <p>Kulit normal, kombinasi, kering, dan sensitif.</p>
      `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/Jancos-09_a3mo9j.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: super_glow_serum.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/YNK_0574_hztcv2.jpg",
  });

  await Product.create({
    product_category_id: whitening_series.id,
    name: "Jankos Glow Luminous Miracle Water Toner",
    description: `
    <p>Luminous Miracle Water Toner diformulasikan dengan 7 ekstrak bunga yang mengandung anti flamasi, anti oksidan, dan anti iritasi. Niacinamide dan Pro Vitamin B5 mencerahkan serta menghidrasi kulit. 
    <br/>Membantu penyerapan lebih baik untuk skincare selanjutnya.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Pro Vitamin B5, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Membantu penyerapan skincare selanjutnya menjadi lebih baik</li>
      <li>Mencerahkan kulit wajah</li>
      <li>Menjaga kelembapan kulit wajah</li>
      <li>Mengatasi kulit kusam</li>
      <li>Memudarkan noda hitam pada kulit wajah</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Setelah mencuci muka, spray pada kapas 2-3 kali lalu aplikasikan ke wajah secara merata, atau spray langsung 20-30 cm pada wajah.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, kering, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/Jancos-04_mybxjg.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });

  const glowing_white_day_cream = await Product.create({
    product_category_id: whitening_series.id,
    name: "Glowing White Day Cream",
    description: `
    <p>Glowing White Day Crem diformulasikan dengan kandungan 7 ekstrak bunga, Vitamin C, dan Kojic Acid menutrisi dan merawat kulit sehingga kulit tampak cerah dan bercahaya. Dilengkapi dengan UV Filter untuk melindungi dari paparan sinar matahari, juga Vitamin E dan Aloe Vera nya menjaga kelembapan kulit. Dengan tekstur seperti foundation, kulit tampak lebih flawless sepanjang hari.</p>
    <br/>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Hyaluronic Acid, Vitamin E, Vitamin C, UV Filter, Kojic Acid dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Dengan tekstur seperti foundation, kulit tampah lebih flawless sepanjang hari</li>
      <li>Menjadikan kulit wajah cerah dan glowing</li>
      <li>Melindungi kulit wajah dari paparan sinar matahari</li>
      <li>Membantu meratakan warna kulit atau hiperpigmentasi</li>
      <li>Memperbaiki dan menghaluskan tekstur kulit</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Ambil secukupnya dengan tangan bersih dan kering, aplikasikan ke seluruh bagian wajah.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, kering, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/Jancos-02_r4mgwk.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: glowing_white_day_cream.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/YNK_0567_wjhx5h.jpg",
  });

  const glowing_white_night_cream = await Product.create({
    product_category_id: whitening_series.id,
    name: "Glowing White Night Cream",
    description: `
    <p>Glowing White Night Cream diformulasikan dengan 7 ekstrak bunga yang mengandung anti inflamasi, anti oksidan, dan anti iritasi. Kandungan Niacinamide membantu mencerahkan kulit, serta diformulasikan khusus untuk menjaga kelembapan sepanjang malam dengan Hydrolyzed Hyaluronic Acid, Aloe Vera dan Pro V
    <br/>itamin B5 untuk kulit tampak cerah, glowing, dan fresh/segar di pagi hari.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Pro Vitamin B5, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Menjadikan kulit wajah cerah dan glowing</li>
      <li>Menghidrasi kulit wajah</li>
      <li>Menjaga kelembapan kulit wajah</li>
      <li>Meregenerasi kulit wajah</li>
      <li>Memudarkan noda hitam pada kulit wajah</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Ambil secukupnya dengan tangan bersih dan kering, aplikasikan ke seluruh bagian wajah.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, kering, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/Jancos-01_zagfjw.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: glowing_white_night_cream.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/YNK_0558_vmlriu.jpg",
  });

  const whitening_packet = await Product.create({
    product_category_id: whitening_series.id,
    name: "Whitening Series",
    is_package: true,
    description: `
    <p>Isi : Facial Wash + Toner + Day Cream + Night Cream </p>
    <p>Set Whitening Series diformulasikan dengan 7 ekstrak bunga yang mengandung anti inflamasi, anti oksidan, dan anti iritasi. Kandungan Niacinamide membantu mencerahkan kulit, serta diformulasikan khusus untuk menjaga kelembapan sepanjang malam dengan Hydrolyzed Hyaluronic Acid, Aloe Vera dan Pro Vitamin B5 untu
    <br/>k kulit tampak cerah dan glowing.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Pro Vitamin B5, Hyaluronic Acid, Vitamin E, Vitamin C, UV Filter, Kojic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Menjadikan kulit wajah cerah dan glowing</li>
      <li>Memudarkan noda hitam pada kulit wajah</li>
      <li>Meregenerasi kulit wajah</li>
      <li>Membantu meratakan warna kulit atau hiperpigmentasi</li>
      <li>Memperbaiki dan menghaluskan tekstur kulit</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Pagi :</p>
    <ol>
      <li>Jankos Glow Sparkling Glow Foam Facial Wash</li>
      <li>Jankos Glow Whitening Series Luminous Miracle Water Toner</li>
      <li>Jankos Glow Whitening Series Super Glow Serum</li>
      <li>Jankos Glow Whitening Series Glowing White Day</li>
    </ol>
    <p>Malam :</p>
    <ol>
      <li>Jankos Glow Sparkling Glow Foam Facial Wash</li>
      <li>Jankos Glow Whitening Series Luminous Miracle Water Toner</li>
      <li>Jankos Glow Whitening Series Super Glow Serum</li>
      <li>Jankos Glow Whitening Series Glowing White Night Cream</li>
    </ol>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kering, kombinasi, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0496_bnz1du.jpg",
    is_starter: true,
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: whitening_packet.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0488_p2npez.jpg",
  });

  await Product.create({
    product_category_id: acne_series.id,
    name: "Acne Clarifying Water Toner",
    description: `
    <p>Acne Clarifying Water Toner merupakan toner dengan tekstur ringan yang mengandung 7 ekstrak dan kombinasi Hydrolyzed Algin dengan Zinc Sulfate yang dapat menutrisi serta merawat kulit, untuk menjaga kelembapan kulit. Salicylic Acid membantu mengeksfoliasi sel kulit mati. Niacinamide membantu mencerahkan noda hitam bekas jerawat. Panthenol dan Aloe Vera menenangkan dan memberi kelembapan pada kul
    <br/>it.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Hydrolyzed Algin, Pro Vitamin B5, Zinc Oxide, Salicylic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Menutrisi kulit serta merawat kulit wajah</li>
      <li>Kulit lebih halus dan mengecilkan pori-pori</li>
      <li>Mencerahkan noda hitam bekas jerawat</li>
      <li>Membantu mengeksfoliasi sel kulit mati</li>
      <li>Meminimalisir munculnya jerawat</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Setelah mencuci muka, aplikasikan Acne Clarifying Toner pada kulit secara merata, tepuk secara pelan agar lebih meresap.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, berminyak, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/Jancos-03_q23oba.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });

  const ultimate_acne_fight_serum = await Product.create({
    product_category_id: acne_series.id,
    name: "Ultimate Acne Fight Serum",
    description: `
    <p>Ultimate Acne Fight Serum diformulasikan khusus untuk kulit berjerawat, dengan 7 ekstrak dan kombinasi Hydrolyzed Algin dengan Zinc Sulfate yang dapat menutrisi serta merawat kulit , melawan bakteri penyebab jerawat. Salicylic Acid membantu mengeksofoliasi sel kulit mati. Niacinamide membantu mencerahkan noda hitam bekas jerawat. Ditambahkan dengan Panthenol, Vitamin E dan Aloe Vera Extract yang dapat memberikan kelembapan dan menyejukan iritasi kulit ringan akibat jerawat.</p>
    <br/>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Hydrolyzed Algin, Pro Vitamin B5, Zinc Oxide, Salicylic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Menutrisi kulit serta merawat kulit wajah</li>
      <li>Meminimalisir munculnya jerawat</li>
      <li>Mencerahkan noda hitam bekas jerawat</li>
      <li>Membantu mengeksfoliasi sel kulit mati</li>
      <li>Menutrisi kulit serta merawat kulit wajah</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Ambil secukupnya dengan tangan bersih dan kering, aplikasikan ke seluruh bagian wajah.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, berminyak, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/Jancos-08_slt1pw.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: ultimate_acne_fight_serum.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0534_mqsmlb.jpg",
  });

  const acne_glowing_white_day_cream = await Product.create({
    product_category_id: acne_series.id,
    name: "Acne Glowing White Day Cream",
    description: `
    <p>Glowing White Day Cream dengan kandungan 7 ekstrak, Vitamin C, dan Kojic Acid menutrisi dan merawat kulit sehingga kulit tampak cerah dan bercahaya. Dilengkapi dengan UV Filter untuk melindungi dari paparan sinar matahari, juga Vitamin E dan Aloe Vera nya menjaga kelembapan kulit. Dengan tekstur sepe
    <br/>rti foundation, kulit tampak lebih flawless sepanjang hari.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, UV Filter, Hyalunoric Acid, Vitamin E, Vitamin C, Kojic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Dengan tekstur seperti foundation, kulit tampah lebih flawless sepanjang hari</li>
      <li>Melindungi kulit wajah dari paparan sinar matahari</li>
      <li>Memperbaiki dan menghaluskan tekstur kulit</li>
      <li>Menutrisi kulit dari dalam</li>
      <li>Membantu meratakan warna kulit atau hiperpigmentasi</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Ambil secukupnya dengan tangan bersih dan kering, aplikasikan ke seluruh bagian wajah.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kering, kombinasi, berminyak, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/Jancos-05_dr4ixn.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: acne_glowing_white_day_cream.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/YNK_0547_mdew29.jpg",
  });

  const anti_acne_white_cream = await Product.create({
    product_category_id: acne_series.id,
    name: "Anti Acne White Night Cream",
    description: `
    <p>Anti Acne Night Cream diformulasikan khusus untuk kulit berjerawat, dengan 7 ekstrak dan kombinasi hydrolyzed Algin dengan Zinc Sulfate yang dapat menutrisi serta merawat kulit, melawan bakteri penyebab jerawat. Salicylic Acid membantu mengeksfoliasi sel kulit mati. Niacinamide membantu mencerahkan noda hitam bekas jerawat. Hydrolyzed Hyaluronic Acid dan Aloe Vera melembabkan dan men
    <br/>yejukan kulit berjerawat.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Hydrolyzed Algin, Zinc Oxide, Salicylic Acid, Hyalunoric Acid, Kojic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Mencerahkan kulit wajah</li>
      <li>Melawan bakteri penyebab jerawat</li>
      <li>Membantu mengeksfoliasi sel kulit mati</li>
      <li>Menutrisi kulit ajah agar lebih sehat</li>
      <li>Melembapkan dan menyejukan kulit berjerawat</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Ambil secukupnya dengan tangan bersih dan kering, aplikasikan ke seluruh bagian wajah</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kombinasi, berminyak, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/Jancos-06_ryupo0.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: anti_acne_white_cream.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851222/jankos/product_image/base-product/YNK_0557_lacseg.jpg",
  });

  const acne_packet = await Product.create({
    product_category_id: acne_series.id,
    name: "Acne Series",
    is_package: true,
    description: `
    <p>Isi : Facial Wash + Toner + Day Cream + Night Cream</p>
    <p>Set Acne Series diformulasikan khusus untuk kulit berjerawat, dengan 7 ekstrak dan kombinasi hydrolyzed Algin dengan Zinc Sulfate yang dapat menutrisi serta merawat kulit, melawan bakteri penyebab jerawat. Salicylic Acid membantu mengeksfoliasi sel kulit mati. Niacinamide membantu mencerahkan noda hitam bekas jerawat. Hydrolyzed Hyaluronic Acid dan Aloe Vera melembabkan dan menyejukan kulit be
    <br/>rjerawat.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Hydrolyzed Algin, Zinc Oxide, Vitamin E, Vitamin C, UV Filter, Salicylic Acid, Hyalunoric Acid, Kojic Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Mencerahkan kulit wajah</li>
      <li>Melawan bakteri penyebab jerawat</li>
      <li>Membantu mengeksfoliasi sel kulit mati</li>
      <li>Meminimalisir munculnya jerawat</li>
      <li>Kulit lebih halus dan mengecilkan pori-pori</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Pagi :</p>
    <ol>
      <li>Jankos Glow Sparkling Glow Foam Facial Wash</li>
      <li>Jankos Glow Acne Clarifying Water Toner</li>
      <li>Jankos Glow Ultimate Acne Fight Serum</li>
      <li>Jankos Glow Whitening Series Glowing White Day Cream</li>
    </ol>
    <p>Malam :</p>
    <ol>
      <li>Jankos Glow Sparkling Glow Foam Facial Wash</li>
      <li>Jankos Glow Acne Clarifying Water Toner</li>
      <li>Jankos Glow Ultimate Acne Fight Serum</li>
      <li>Jankos Glow Anti Acne Night Cream</li>
    </ol>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kering, kombinasi, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0467_m0oo2p.jpg",
    is_starter: true,
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: acne_packet.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0478_y6oht7.jpg",
  });

  const sparkling_glow_foam_product = await Product.create({
    product_category_id: sparkling_glow_foam.id,
    name: "Sparkling Glow Foam Facial Wash",
    is_starter: false,
    description: `
    <p>Sparkling Glow Foam Facial Wash Mengandung 7 ekstrak bunga, Niacinamide, dan Hydrolyzed Hyaluronic Acid membantu membersihkan kotoran, mencerahkan dan menjaga kelembapan kulit. Dipadukan dengan Salicylic Ac
    <br/>id untuk mengangkat kulit mati sehingga kulit lebih bersih dan sehat.</p>
    <p><b>Kandungan</b></p>
    <p>7 ekstrak bunga, Niacinamide, Pro Vitamin B5, Salicylic Acid, Hyalunoric Acid, dan Aloe Barbadensis Leaf Extract.</p>
    <br/>
    <p><b>Manfaat :</b></p>
    <ul>
      <li>Membersihkan kulit serta mengangkat sel kulit mati, sebum dan kotoran pada wajah</li>
      <li>Menghidrasi kulit wajah</li>
      <li>Menenangkan kulit wajah serta tanpa membuat kulit kering, tertarik dan tidak merusak skin barrier kulit</li>
    </ul>
    <br/>
    <p><b>Cara Pemakaian</b></p>
    <p>Basahi wajah dengan air. Tekan pump botol, keluarkan foam secukupnya pada telapak tangan, lalu usapkan foam ke seluruh wajah, bilas dengan air bersih.</p>
    <br/>
    <p><b>Cocok untuk</b></p>
    <p>Kulit normal, kering, kombinasi, berminyak, dan sensitif.</p>
    `,
    price: 60000,
    weight: 10,
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851223/jankos/product_image/base-product/Jancos-07_sid6vu.jpg",
    short_description:
      "Lorem ipsum dolor sit amet consectetur lorem ipsum dolor",
  });
  await ProductImage.create({
    product_id: sparkling_glow_foam_product.id,
    url: "https://res.cloudinary.com/rizaldiariif/image/upload/v1663851221/jankos/product_image/base-product/YNK_0536_qesk3r.jpg",
  });

  await MarketingMaterial.bulkCreate([
    {
      title: "Marketing Material 1",
      code: "product",
      text: "lorem ipsum dolor sit amet",
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1652722895/demo-image_rln8fq.png",
    },
    {
      title: "Marketing Material 2",
      code: "promotion",
      text: "lorem ipsum dolor sit amet",
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1652722895/demo-image_rln8fq.png",
    },
  ]);

  await OrderStatus.bulkCreate([
    {
      name: "Menunggu Pembayaran",
      code: "menunggu_pembayaran",
    },
    {
      name: "Request Pickup",
      code: "request_pickup",
    },
    {
      name: "Processing",
      code: "processing",
    },
    {
      name: "Picked",
      code: "picked",
    },
    {
      name: "Delivering",
      code: "delivering",
    },
    {
      name: "Delivered",
      code: "delivered",
    },
    {
      name: "Problem",
      code: "problem",
    },
    {
      name: "Returned",
      code: "returned",
    },
    {
      name: "Returning",
      code: "returning",
    },
    {
      name: "Expired",
      code: "expired",
    },
  ]);

  const superadmin_level = await AdminLevel.create({
    name: "Super Admin",
    hierarchy_number: 999,
  });
  await AdminLevel.create({
    name: "Admin",
    hierarchy_number: 1,
  });

  await Admin.create({
    name: "Super Admin",
    admin_level_id: superadmin_level.id,
    email: "superadmin@gmail.com",
    password: "ABCabc123_",
  });

  for (const article of articles) {
    await Article.create(article);
  }

  await Article.create({
    title:
      "Tips Eksfoliasi Sesuai dengan Jenis Kulit, dari Kering Hingga Sensitif",
    author: "Meidinda Siti A.",
    text: "<p>Testing</p>",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1654359256/jankos/article/thumbnails/image_thumbnail_1654359244772_Rectangle%20240%20%281%29.png",
  });

  await Testimony.bulkCreate([
    {
      name: "Raisa Athirah",
      text: "Aku coba pakai ini pas aku masih belum ngerti mau nya kulit ku tu apa, gladly beneran cocok! Bruntusan aku juga udh mulai hilang dan wajahnya jadi keliatan lebih fresh. Worth to try! :)”",
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1654443773/jankos/Ellipse_7_1_qsqtyw.png",
    },
    {
      name: "Tiara Permatasari",
      text: "Basically, kulitku itu sensitive banget jadi gampang jerawatan. Produk Jankos Acne Fight ini cocok bgt di kulitku 😍 Teksturnya gel ringan ada wangi yang calming.",
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1654443771/jankos/Ellipse_7_2_nwgkgz.png",
    },
    {
      name: "Christina Kim",
      text: "“Salah satu serum brightening lokal yang aku suka banget, untuk efeknya membantu membuat kulit ku lebih cerah karna ada efek niacinamidenya, oh iya ga ada efek samping apapun di kulit ku so far",
      thumbnail:
        "https://res.cloudinary.com/rizaldiariif/image/upload/v1654443770/jankos/Ellipse_7_3_a5moij.png",
    },
  ]);

  for (const page_content of page_contents) {
    await PageContent.create(page_content);
  }

  for (const reason_item of reason_items) {
    await ReasonItem.create(reason_item);
  }

  res.status(200).json({ message: "db synced!" });
};
