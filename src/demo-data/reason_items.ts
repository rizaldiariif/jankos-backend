import { ReasonItemCreationAttributes } from "../models/reason_item";

export default [
  {
    title: "Jasmine",
    text: "Sebagai antiseptik alami untuk kulit dan mengembalikan kelembaban kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662445885/jankos/testimony/thumbnails/image_thumbnail_1662445885071_Group%20447.png",
  },
  {
    title: "Peony",
    text: "Membantu proses pengangkatan sel kulit mati dan pembentukkan sel-sel kulit baru",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662445935/jankos/testimony/thumbnails/image_thumbnail_1662445935917_Group%20450.png",
  },
  {
    title: "Japanese Cherry",
    text: "Mencegah produksi melanin berlebih yang membuat warna kulit tidak merata",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662445961/jankos/testimony/thumbnails/image_thumbnail_1662445961839_Group%20453.png",
  },
  {
    title: "Rose",
    text: "Melembabkan kulit dengan alami yang cocok untuk segala jenis kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662445981/jankos/testimony/thumbnails/image_thumbnail_1662445981869_Group%20455.png",
  },
  {
    title: "Lavender",
    text: "Menyejukkan kulit yang teriritasi dan mengunci kelembaban kulit dengan sempurna",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446031/jankos/testimony/thumbnails/image_thumbnail_1662446031040_Group%20459.png",
  },
  {
    title: "Neroli",
    text: "Mempercepat proses regenerasi sel kulit, memudarkan noda hitam, dan meningkatkan elastisitas kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446081/jankos/testimony/thumbnails/image_thumbnail_1662446081231_Group%20462.png",
  },
  {
    title: "Elderberry",
    text: "Antioksidan dan menangkal radikal bebas",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446099/jankos/testimony/thumbnails/image_thumbnail_1662446099650_Group%20465.png",
  },
  {
    title: "Niacinamide",
    text: "Menutrisi, mencerahkan, dan memperbaiki tekstur kulit dari pori-pori, noda jerawat, dll",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446118/jankos/testimony/thumbnails/image_thumbnail_1662446118497_Group%20468.png",
  },
  {
    title: "Hyaluronic Acid",
    text: "Memberikan kelembaban, mengunci minyak, dan mengembalikan kelembaban kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446118/jankos/testimony/thumbnails/image_thumbnail_1662446118497_Group%20468.png",
  },
  {
    title: "Salicylic Acid",
    text: "Membantu mencerahkan noda hitam dan mengeksfoliasi sel kulit mati",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446118/jankos/testimony/thumbnails/image_thumbnail_1662446118497_Group%20468.png",
  },
  {
    title: "Hydrolized Algin",
    text: "Menutrisi, merawat kulit, dan melawan bakteri penyebab jerawat",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446118/jankos/testimony/thumbnails/image_thumbnail_1662446118497_Group%20468.png",
  },
  {
    title: "Zinc Sulfate",
    text: "Menutrisi, merawat kulit, dan melawan bakteri penyebab jerawat",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446118/jankos/testimony/thumbnails/image_thumbnail_1662446118497_Group%20468.png",
  },
  {
    title: "Aloe Vera",
    text: "Menyejukkan kulit yang teriritasi",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446220/jankos/testimony/thumbnails/image_thumbnail_1662446220154_Group%20484.png",
  },
  {
    title: "Vit C",
    text: "Mencerahkan kulit dan melindungi wajah dari efek sinar UV",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446241/jankos/testimony/thumbnails/image_thumbnail_1662446241593_Group%20487.png",
  },
  {
    title: "Vit E",
    text: "Merawat kulit, memperbaiki pigmen dan tekstur kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446259/jankos/testimony/thumbnails/image_thumbnail_1662446259774_Group%20490.png",
  },
  {
    title: "Panthenol",
    text: "Membuat kulit lebih lembab, halus, dan meningkatkan elastisitas kulit",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446284/jankos/testimony/thumbnails/image_thumbnail_1662446284127_Group%20468.png",
  },
  {
    title: "Pro Vit B5",
    text: "Menjaga kelembaban kulit dan merawat kulit kusam",
    thumbnail:
      "https://res.cloudinary.com/rizaldiariif/image/upload/v1662446311/jankos/testimony/thumbnails/image_thumbnail_1662446311351_Group%20497.png",
  },
] as ReasonItemCreationAttributes[];
