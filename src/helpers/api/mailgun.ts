import Mailgun from "mailgun-js";
import dotenv from "dotenv";

dotenv.config();

if (typeof process.env.MAILGUN_API_KEY !== "string") {
  throw new Error("MAILGUN_API_KEY is not defined!");
}
if (typeof process.env.MAILGUN_DOMAIN !== "string") {
  throw new Error("MAILGUN_DOMAIN is not defined!");
}
if (typeof process.env.MAILGUN_FROM_EMAIL !== "string") {
  throw new Error("MAILGUN_FROM_EMAIL is not defined!");
}

const mailgun_client = new Mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});

const sendEmail = async (to: string, subject: string, message: string) => {
  try {
    const data = await mailgun_client.messages().send({
      from: process.env.MAILGUN_FROM_EMAIL,
      to,
      subject,
      html: message,
    });
    return data;
  } catch (error: any) {
    console.error(error);
    throw new Error(error.message);
  }
};

export { mailgun_client, sendEmail };
