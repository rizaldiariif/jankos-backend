import { v2 as cloudinary } from "cloudinary";
import { CloudinaryStorage } from "multer-storage-cloudinary";
import multer from "multer";

export const uploader = (folder_name: string) =>
  multer({
    storage: new CloudinaryStorage({
      cloudinary: cloudinary,
      params: async (_req, file) => {
        return {
          folder: `jankos/${folder_name}`,
          public_id: `${file.fieldname}_${Date.now()}_${
            file.originalname.split(".")[0]
          }`,
        };
      },
    }),
  });
