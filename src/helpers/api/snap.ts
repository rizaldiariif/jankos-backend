import axios from "axios";
import dotenv from "dotenv";

dotenv.config();

if (typeof process.env.MIDTRANS_SNAP_BASE_URL !== "string") {
  throw new Error("MIDTRANS_SNAP_BASE_URL is not defined!");
}
if (typeof process.env.MIDTRANS_SERVER_KEY !== "string") {
  throw new Error("MIDTRANS_SERVER_KEY is not defined!");
}

const snapClient = axios.create({
  baseURL: process.env.MIDTRANS_SNAP_BASE_URL,
  headers: {
    Authorization: `Basic ${Buffer.from(
      `${process.env.MIDTRANS_SERVER_KEY}:`
    ).toString("base64")}`,
  },
});

export { snapClient };
