import axios from "axios";
import { format } from "date-fns";
import dotenv from "dotenv";
// import { CustomError } from "../../errors/custom-error";
import { MemberShippingAddress } from "../../models/member_shipping_address";
import { Order } from "../../models/order";
import { OrderDetail } from "../../models/order_detail";
import { Product } from "../../models/product";
// import { geocoder } from "./geocoder";
// @ts-ignore
import { combineLoop } from "box-dimension-calculator";
import shortUUID from "short-uuid";

dotenv.config();

if (typeof process.env.SHIPDEO_AUTH_URL !== "string") {
  throw new Error("SHIPDEO_AUTH_URL is not defined!");
}
if (typeof process.env.SHIPDEO_MAIN_URL !== "string") {
  throw new Error("SHIPDEO_MAIN_URL is not defined!");
}
if (typeof process.env.SHIPDEO_CLIENT_ID !== "string") {
  throw new Error("SHIPDEO_CLIENT_ID is not defined!");
}
if (typeof process.env.SHIPDEO_CLIENT_SECRET !== "string") {
  throw new Error("SHIPDEO_CLIENT_SECRET is not defined!");
}

export const shipdeoClient = async () => {
  try {
    const response = await axios.post(
      `${process.env.SHIPDEO_AUTH_URL}/oauth2/connect/token`,
      {
        client_id: process.env.SHIPDEO_CLIENT_ID,
        client_secret: process.env.SHIPDEO_CLIENT_SECRET,
        grant_type: "client_credentials",
      }
    );

    const { access_token } = response.data;

    return axios.create({
      baseURL: process.env.SHIPDEO_MAIN_URL,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    });
  } catch (error: any) {
    console.log(error.response.data);
    throw new Error("Shipdeo Instance Error!");
  }
};

export const checkServicePrice = async (
  address: MemberShippingAddress,
  carts: { product: Product; quantity: number }[],
  courier_code: string,
  service: string
) => {
  const shipdeo_client = await shipdeoClient();

  if (typeof process.env.SHIPDEO_JANKOS_COMPLETE_ADDRESS !== "string") {
    throw new Error("SHIPDEO_JANKOS_COMPLETE_ADDRESS is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_POSTAL_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_POSTAL_CODE is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_SUBDISTRICT_CODE is not set!");
  }

  // const [geocode_jankos] = await geocoder.geocode(
  //   process.env.SHIPDEO_JANKOS_COMPLETE_ADDRESS
  // );

  const {
    data: { data: address_jankos },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE,
  });

  const {
    data: { data: couriers },
  } = await shipdeo_client.get("/v1/couriers");

  const { data: pricing } = await shipdeo_client.post("/v1/couriers/pricing", {
    couriers: couriers.list.map((courier: any) => courier.code),
    is_cod: false,
    // origin_lat: geocode_jankos.latitude,
    // origin_long: geocode_jankos.longitude,
    origin_province_code: address_jankos.city.province.province_code,
    origin_province_name: address_jankos.city.province.province_name,
    origin_city_code: address_jankos.city.city_code,
    origin_city_name: address_jankos.city.city_name,
    origin_subdistrict_code: address_jankos.subdistrict_code,
    origin_subdistrict_name: address_jankos.subdistrict_name,
    origin_postal_code: process.env.SHIPDEO_JANKOS_POSTAL_CODE,
    // destination_lat: address.latitude,
    // destination_long: address.longitude,
    destination_province_code: address.province_code,
    destination_province_name: address.province_name,
    destination_city_code: address.city_code,
    destination_city_name: address.city_name,
    destination_subdistrict_code: address.subdistrict_code,
    destination_subdistrict_name: address.subdistrict_name,
    destination_postal_code: address.postal_code,
    items: carts.map((item) => {
      return {
        name: item.product.name,
        description: item.product.description,
        weight: item.product.weight,
        weight_uom: "gr",
        qty: item.quantity,
        value: item.product.price * item.quantity,
        width: item.product.width,
        height: item.product.height,
        length: item.product.length,
        is_wood_package: false,
        dimension_uom: "cm",
      };
    }),
    isCallWeight: true,
  });

  const shipping_service = pricing.data.find(
    (p: any) =>
      p.errors === undefined &&
      p.courierCode === courier_code &&
      p.service === service
  );

  return shipping_service;
};

export const shipOrder = async (order: Order) => {
  const shipdeo_client = await shipdeoClient();

  if (typeof process.env.SHIPDEO_JANKOS_COMPLETE_ADDRESS !== "string") {
    throw new Error("SHIPDEO_JANKOS_COMPLETE_ADDRESS is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_POSTAL_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_POSTAL_CODE is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE !== "string") {
    throw new Error("SHIPDEO_JANKOS_SUBDISTRICT_CODE is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_CONTACT_NAME !== "string") {
    throw new Error("SHIPDEO_JANKOS_CONTACT_NAME is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_CONTACT_PHONE !== "string") {
    throw new Error("SHIPDEO_JANKOS_CONTACT_PHONE is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_CONTACT_ADDRESS !== "string") {
    throw new Error("SHIPDEO_JANKOS_CONTACT_ADDRESS is not set!");
  }

  if (typeof process.env.SHIPDEO_JANKOS_CONTACT_EMAIL !== "string") {
    throw new Error("SHIPDEO_JANKOS_CONTACT_EMAIL is not set!");
  }

  const {
    data: { data: address_jankos },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE,
  });

  // const [geocode_jankos] = await geocoder.geocode({
  //   // address: `${process.env.SHIPDEO_JANKOS_CONTACT_ADDRESS}, ${address_jankos.subdistrict_name}, ${address_jankos.city.city_name}, ${address_jankos.city.province.province_name}`,
  //   country: "Indonesia",
  //   zipcode: `${process.env.SHIPDEO_JANKOS_POSTAL_CODE}`,
  // });

  const order_details = await OrderDetail.findAll({
    where: { order_id: order.id },
  });

  // const subtotal_orders = order_details.map(order_detail=>order_detail.price * order_detail.quantity).reduce((total_value,current_total_value)=>total_value+current_total_value,0);

  const order_total = order_details
    .map((order_detail) => {
      return {
        price: order_detail.price,
        width: order_detail.width,
        height: order_detail.height,
        length: order_detail.length,
        weight: order_detail.weight,
        quantity: order_detail.quantity,
      };
    })
    .reduce(
      (order_total, current_order) => {
        const item_dimensions: {
          l: number;
          w: number;
          h: number;
        }[] = [];

        for (let index = 1; index <= current_order.quantity; index++) {
          item_dimensions.push({
            h: current_order.height,
            l: current_order.length,
            w: current_order.width,
          });
        }

        const shipping_dimension = combineLoop(item_dimensions);

        return {
          price:
            order_total.price + current_order.price * current_order.quantity,
          width: order_total.width + shipping_dimension[0].w,
          height: order_total.height + shipping_dimension[0].h,
          length: order_total.length + shipping_dimension[0].l,
          weight:
            order_total.weight + current_order.weight * current_order.quantity,
          quantity: order_total.quantity + current_order.quantity,
        };
      },
      {
        price: 0,
        width: 0,
        height: 0,
        length: 0,
        weight: 0,
        quantity: 0,
      }
    );

  console.log(format(new Date(), "MM/dd/yyyy hh:mm:ss aa xx"));

  const {
    data: { data: shipping_order },
  } = await shipdeo_client.post("/v1/couriers/orders", {
    courier: order.shipping_courier_code,
    courier_service: order.shipping_courier_service,
    order_number: shortUUID().fromUUID(order.id).toUpperCase(),
    is_cod: false,
    delivery_type: "pickup",
    delivery_time: `${format(new Date(), "MM/dd/yyyy hh:mm:ss aa xx")}`,
    is_send_company: true,
    // origin_lat: geocode_jankos.latitude,
    // origin_long: geocode_jankos.longitude,
    origin_subdistrict_code: address_jankos.subdistrict_code,
    origin_subdistrict_name: address_jankos.subdistrict_name,
    origin_city_code: address_jankos.city.city_code,
    origin_city_name: address_jankos.city.city_name,
    origin_province_code: address_jankos.city.province.province_code,
    origin_province_name: address_jankos.city.province.province_name,
    origin_contact_name: process.env.SHIPDEO_JANKOS_CONTACT_NAME,
    origin_contact_phone: process.env.SHIPDEO_JANKOS_CONTACT_PHONE,
    origin_contact_address: process.env.SHIPDEO_JANKOS_CONTACT_ADDRESS,
    origin_contact_email: process.env.SHIPDEO_JANKOS_CONTACT_EMAIL,
    origin_note: "",
    origin_postal_code: process.env.SHIPDEO_JANKOS_POSTAL_CODE,
    // destination_lat: order.latitude,
    // destination_long: order.longitude,
    destination_subdistrict_code: order.subdistrict_code,
    destination_subdistrict_name: order.subdistrict_name,
    destination_city_code: order.city_code,
    destination_city_name: order.city_name,
    destination_province_code: order.province_code,
    destination_province_name: order.province_name,
    destination_contact_name: order.contact_name,
    destination_contact_phone: order.contact_phone,
    destination_contact_address: order.contact_address,
    destination_contact_email: order.contact_email,
    destination_note: order.address_note,
    destination_postal_code: order.postal_code,
    delivery_note: "Cosmetics",
    items: order_details.map((order_detail) => {
      return {
        name: order_detail.name,
        description: order_detail.description,
        weight: order_detail.weight,
        weight_uom: "gr",
        qty: order_detail.quantity,
        value: order_detail.price,
        width: order_detail.width,
        height: order_detail.height,
        length: order_detail.length,
        dimension_uom: "cm",
        total_value: order_detail.price * order_detail.quantity,
      };
    }),
    transaction: {
      subtotal: order_total.price,
      shipping_charge: order.shipping_price,
      fee_insurance: 0,
      is_insuranced: false,
      discount: 0,
      total_value: order_total.price + order.shipping_price,
      total_cod: 0,
      weight: order_total.weight,
      height: order_total.height,
      length: order_total.length,
      width: order_total.width,
      coolie: 1,
      package_category: "Cosmetics",
      package_content: "Cosmetics",
    },
  });

  console.log({ shipping_order });

  order.set("shipping_id", shipping_order._id);
  order.set("airwaybill_number", shipping_order.tracking_info.airwaybill);

  await order.save();

  return order;
};

export const checkAvailableCouriers = async (
  address_destination: MemberShippingAddress,
  carts: { product: Product; quantity: number }[]
) => {
  const shipdeo_client = await shipdeoClient();

  const {
    data: { data: address_jankos },
  } = await shipdeo_client.post("/v1/master/locations/by-code", {
    code: process.env.SHIPDEO_JANKOS_SUBDISTRICT_CODE,
  });

  const {
    data: { data: couriers },
  } = await shipdeo_client.get("/v1/couriers");

  const cart_items: any[] = [];
  carts.forEach((item) => {
    cart_items.push({
      name: item.product.name,
      description: item.product.description,
      weight: item.product.weight,
      weight_uom: "gr",
      qty: item.quantity,
      value: item.product.price * item.quantity,
      width: item.product.weight,
      height: item.product.height,
      length: item.product.length,
      is_wood_package: false,
      dimension_uom: "cm",
    });
  });

  const { data: pricing } = await shipdeo_client.post("/v1/couriers/pricing", {
    couriers: couriers.list.map((courier: any) => courier.code),
    is_cod: false,
    // origin_lat: geocode_response_jankos.latitude,
    // origin_long: geocode_response_jankos.longitude,
    origin_province_code: address_jankos.city.province.province_code,
    origin_province_name: address_jankos.city.province.province_name,
    origin_city_code: address_jankos.city.city_code,
    origin_city_name: address_jankos.city.city_name,
    origin_subdistrict_code: address_jankos.subdistrict_code,
    origin_subdistrict_name: address_jankos.subdistrict_name,
    origin_postal_code: process.env.SHIPDEO_JANKOS_POSTAL_CODE,
    // destination_lat: geocode_response_destination.latitude,
    // destination_long: geocode_response_destination.longitude,
    destination_province_code: address_destination.province_code,
    destination_province_name: address_destination.province_name,
    destination_city_code: address_destination.city_code,
    destination_city_name: address_destination.city_name,
    destination_subdistrict_code: address_destination.subdistrict_code,
    destination_subdistrict_name: address_destination.subdistrict_name,
    destination_postal_code: address_destination.postal_code,
    items: cart_items,
    isCallWeight: true,
  });

  const available_pricing = pricing.data.filter(
    (p: any) => p.errors === undefined
  );

  return available_pricing;
};
