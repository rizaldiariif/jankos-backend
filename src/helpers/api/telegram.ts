import TelegramBot from "node-telegram-bot-api";
import dotenv from "dotenv";

dotenv.config();

if (typeof process.env.TELEGRAM_BOT_TOKEN !== "string") {
  throw new Error("TELEGRAM_BOT_TOKEN is not defined!");
}
if (typeof process.env.TELEGRAM_CHAT_ID !== "string") {
  throw new Error("TELEGRAM_CHAT_ID is not defined!");
}

const bot = new TelegramBot(process.env.TELEGRAM_BOT_TOKEN);

const sendCustomMessage = (message: string) => {
  bot.sendMessage(process.env.TELEGRAM_CHAT_ID!, message);
};

export { bot, sendCustomMessage };
