import axios from "axios";
import dotenv from "dotenv";

dotenv.config();

if (typeof process.env.MIDTRANS_IRIS_BASE_URL !== "string") {
  throw new Error("MIDTRANS_IRIS_BASE_URL is not defined!");
}
if (typeof process.env.MIDTRANS_IRIS_CREATOR_API_KEY !== "string") {
  throw new Error("MIDTRANS_IRIS_CREATOR_API_KEY is not defined!");
}
if (typeof process.env.MIDTRANS_IRIS_APPROVER_API_KEY !== "string") {
  throw new Error("MIDTRANS_IRIS_APPROVER_API_KEY is not defined!");
}

const irisCreatorClient = axios.create({
  baseURL: process.env.MIDTRANS_IRIS_BASE_URL,
  headers: {
    Authorization: `Basic ${Buffer.from(
      `${process.env.MIDTRANS_IRIS_CREATOR_API_KEY}:`
    ).toString("base64")}`,
  },
});

const irisApproverClient = axios.create({
  baseURL: process.env.MIDTRANS_IRIS_BASE_URL,
  headers: {
    Authorization: `Basic ${Buffer.from(
      `${process.env.MIDTRANS_IRIS_APPROVER_API_KEY}:`
    ).toString("base64")}`,
  },
});

export { irisCreatorClient, irisApproverClient };
