import NodeGeocoder, { Providers } from "node-geocoder";

const provider = process.env.SHIPDEO_GEOCODING_PROVIDER as Providers;

if (typeof provider !== "string") {
  throw new Error("SHIPDEO_GEOCODING_PROVIDER must be defined.");
}

if (typeof process.env.SHIPDEO_GEOCODING_TOKEN !== "string") {
  throw new Error("SHIPDEO_GEOCODING_TOKEN must be defined.");
}

if (typeof process.env.SHIPDEO_GEOCODING_TOKEN !== "string") {
  throw new Error("SHIPDEO_GEOCODING_TOKEN must be defined.");
}

const geocoder = NodeGeocoder({
  provider,
  apiKey: process.env.SHIPDEO_GEOCODING_TOKEN,
});

export { geocoder };
