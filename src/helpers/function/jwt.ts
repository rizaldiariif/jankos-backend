import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

export interface TokenPayload {
  id: string;
  email: string;
}

export interface TokenVerificationPayload {
  id: string;
}

export const tokenize = (payload: TokenPayload) => {
  if (typeof process.env.APP_JWT_KEY !== "string") {
    throw new Error("APP_JWT_KEY must be defined.");
  }

  return jwt.sign(payload, process.env.APP_JWT_KEY);
};

export const tokenizeVerification = (payload: TokenVerificationPayload) => {
  if (typeof process.env.APP_JWT_KEY !== "string") {
    throw new Error("APP_JWT_KEY must be defined.");
  }

  return jwt.sign(payload, process.env.APP_JWT_KEY, {
    expiresIn: "1d",
  });
};
