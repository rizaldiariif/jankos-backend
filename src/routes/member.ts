import { Router, Request, Response } from "express";
import {
  currentMember,
  detail,
  process_forget_password,
  request_forget_password,
  signin,
  signup,
  update_my_password,
  update_my_profile,
} from "../controller/member";
import { advancedResults } from "../middlewares/advanced-result";

import { Member } from "../models/member";
import { validateRequest } from "../middlewares/validate-request";
import { currentMember as currentMemberMiddleware } from "../middlewares/current-member";
import { body } from "express-validator";
import { uploader } from "../helpers/api/cloudinary";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { requireMember } from "../middlewares/require-member";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  currentAdmin,
  requireAdmin,
  advancedResults(Member, ["member_level"]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

// AUTHENTICATIONS
router.post(
  "/auth/signup",
  uploader("member/images").fields([
    // { name: "image_profile", maxCount: 1 },
    { name: "image_id_card", maxCount: 1 },
  ]),
  [
    body("email").notEmpty().withMessage("Email is required."),
    body("password").notEmpty().withMessage("Password is required."),
    body("name").notEmpty().withMessage("Name is required."),
    body("phone").notEmpty().withMessage("Phone is required."),
    body("address").notEmpty().withMessage("Address is required."),
  ],
  validateRequest,
  signup
);
router.post(
  "/auth/signin",
  [
    body("email").notEmpty().withMessage("Email is required."),
    body("password").notEmpty().withMessage("Password is required."),
  ],
  validateRequest,
  signin
);
router.get("/auth/me", currentMemberMiddleware, currentMember);
router.post(
  "/auth/request-forget-password",
  [body("email").notEmpty().bail().isEmail()],
  validateRequest,
  request_forget_password
);
router.post(
  "/auth/process-forget-password",
  [body("password").notEmpty(), body("verification_token").notEmpty()],
  validateRequest,
  process_forget_password
);

router.post(
  "/update-my-profile",
  currentMemberMiddleware,
  requireMember,
  [
    body("name").notEmpty(),
    body("phone").notEmpty(),
    body("address").notEmpty(),
  ],
  validateRequest,
  update_my_profile
);

router.post(
  "/update-my-password",
  currentMemberMiddleware,
  requireMember,
  [body("current_password").notEmpty(), body("password").notEmpty()],
  validateRequest,
  update_my_password
);

router.get("/:id", currentAdmin, requireAdmin, detail);

export { router as memberRouter };
