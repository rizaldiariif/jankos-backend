import { Request, Response, Router } from "express";
import {
  create,
  currentAdmin,
  delete_admin,
  signin,
  update,
} from "../controller/admin";

import { validateRequest } from "../middlewares/validate-request";
import { body } from "express-validator";
import { currentAdmin as currentAdminMiddleware } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { advancedResults } from "../middlewares/advanced-result";
import { Admin } from "../models/admin";

const router = Router();

router.get(
  "/",
  currentAdminMiddleware,
  requireAdmin,
  advancedResults(Admin, ["admin_level"]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdminMiddleware,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("email").notEmpty(),
    body("password").notEmpty(),
    body("admin_level_id").notEmpty(),
  ],
  validateRequest,
  create
);

// AUTHENTICATIONS
router.post(
  "/auth/signin",
  [
    body("email").notEmpty().withMessage("Email is required."),
    body("password").notEmpty().withMessage("Password is required."),
  ],
  validateRequest,
  signin
);
router.get("/auth/me", currentAdminMiddleware, currentAdmin);

router.post(
  "/:id",
  currentAdminMiddleware,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("email").notEmpty(),
    body("admin_level_id").notEmpty(),
  ],
  validateRequest,
  update
);

router.delete("/:id", currentAdminMiddleware, requireAdmin, delete_admin);

export { router as adminRouter };
