import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_product, update } from "../controller/product";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";

import { Product } from "../models/product";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  advancedResults(Product, [
    {
      association: "product_level_prices",
      include: ["member_level"],
      order: [["createdAt", "ASC"]],
    },
    {
      association: "product_images",
      order: [["createdAt", "ASC"]],
    },
  ]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("product/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("name").notEmpty(),
    body("price").notEmpty(),
    body("weight").notEmpty(),
    body("length").notEmpty(),
    body("width").notEmpty(),
    body("height").notEmpty(),
    body("description").notEmpty(),
  ],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("product/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("name").notEmpty(),
    body("price").notEmpty(),
    body("weight").notEmpty(),
    body("length").notEmpty(),
    body("width").notEmpty(),
    body("height").notEmpty(),
    body("description").notEmpty(),
    body("product_level_prices").notEmpty(),
  ],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_product);

export { router as productRouter };
