import { Router, Request, Response } from "express";
import { create, delete_certification } from "../controller/certification";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { Certification } from "../models/certification";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  advancedResults(Certification),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("certification/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  validateRequest,
  create
);

router.delete("/:id", currentAdmin, requireAdmin, delete_certification);

export { router as certificationRouter };
