import { Router } from "express";
import { body, param, query } from "express-validator";
import {
  add_shipping_address,
  change_password,
  check_referral_code,
  check_shipping_price,
  check_shipping_price_signup,
  commistion_payout,
  create_withdrawal,
  download_invoice,
  forget_password,
  iris_notification,
  listing_couriers,
  listing_locations,
  listing_order,
  listing_postal_codes,
  make_order,
  my_address,
  my_orders,
  my_order_detail,
  my_transaction_monthly,
  pickup_order,
  redeem_point,
  send_email_support,
  shipping_label,
  singup,
  update_my_account,
} from "../controller/flow";
import { uploader } from "../helpers/api/cloudinary";
import { currentAdmin } from "../middlewares/current-admin";
import { currentMember } from "../middlewares/current-member";
import { requireAdmin } from "../middlewares/require-admin";
import { requireMember } from "../middlewares/require-member";
import { validateRequest } from "../middlewares/validate-request";

const router = Router();

// UNIVERSAL GETTER
router.post(
  "/signup",
  uploader("member/id_card").fields([{ name: "image_id_card", maxCount: 1 }]),
  [
    body("name").notEmpty(),
    body("phone").notEmpty(),
    body("email").notEmpty(),
    body("password").notEmpty(),
    body("bank_name").notEmpty(),
    body("bank_account_name").notEmpty(),
    body("bank_account_number").notEmpty(),
    body("address").notEmpty(),
    body("member_level_id").notEmpty(),
    // body("product_id").notEmpty(),
    // body("quantity").notEmpty(),
    body("shipping_address").notEmpty(),
    body("shipping_type").notEmpty(),
    body("products").notEmpty(),
    body("social_media_url").notEmpty(),
    // body("marketplace_url").notEmpty(),
  ],
  validateRequest,
  singup
);

router.post(
  "/listing-order",
  [body("email").notEmpty(), body("password").notEmpty()],
  validateRequest,
  listing_order
);

router.get("/listing-couriers", listing_couriers);

router.get(
  "/listing-locations",
  [query("type").notEmpty(), query("name").optional()],
  listing_locations
);

router.get(
  "/listing-postal-codes",
  [query("name").optional()],
  listing_postal_codes
);

router.post(
  "/check-shipping-price",
  [body("items").notEmpty(), body("member_shipping_address_id").notEmpty()],
  validateRequest,
  check_shipping_price
);

router.post(
  "/check-shipping-price-signup",
  [
    body("destination_subdistrict_code").notEmpty(),
    body("destination_postal_code").notEmpty(),
    // body("product_id").notEmpty(),
    // body("quantity").notEmpty(),
    body("products").notEmpty(),
  ],
  validateRequest,
  check_shipping_price_signup
);

router.get(
  "/my-orders",
  currentMember,
  requireMember,
  [query("code").default("menunggu_pembayaran")],
  validateRequest,
  my_orders
);

router.get(
  "/my-order-detail",
  currentMember,
  requireMember,
  [query("id").notEmpty()],
  validateRequest,
  my_order_detail
);

router.get(
  "/check-referral-code",
  [query("referral_code").notEmpty()],
  validateRequest,
  check_referral_code
);

router.post(
  "/redeem-point",
  currentMember,
  requireMember,
  [body("product_id").notEmpty(), body("shipping_address_id").notEmpty()],
  validateRequest,
  redeem_point
);

router.get("/my-address", currentMember, requireMember, my_address);

router.post(
  "/make-order",
  currentMember,
  requireMember,
  [
    body("items").notEmpty(),
    // body("member_shipping_address_id").notEmpty(),
    // body("shipping_courier_code").notEmpty(),
    // body("shipping_courier_service").notEmpty(),
    body("shipping_type").notEmpty(),
  ],
  validateRequest,
  make_order
);

router.post(
  "/commission-payout",
  currentMember,
  requireMember,
  [body("amount").isInt({ min: 1 }).notEmpty()],
  validateRequest,
  commistion_payout
);

router.post(
  "/update-my-account",
  currentMember,
  requireMember,
  uploader("member/id_card").fields([{ name: "image_id_card", maxCount: 1 }]),
  [
    body("name").notEmpty(),
    body("phone").notEmpty(),
    body("email").notEmpty(),
    body("bank_name").notEmpty(),
    body("bank_account_name").notEmpty(),
    body("bank_account_number").notEmpty(),
    body("address").notEmpty(),
  ],
  validateRequest,
  update_my_account
);

router.get(
  "/shipping-label/:order_id",
  // currentAdmin,
  // requireAdmin,
  shipping_label
);

router.get("/pickup-order/:order_id", currentAdmin, requireAdmin, pickup_order);

router.post(
  "/create-withdrawal",
  currentMember,
  requireMember,
  [body("amount").notEmpty()],
  validateRequest,
  create_withdrawal
);

router.post(
  "/iris-notification",
  [body("reference_no").notEmpty(), body("status").notEmpty()],
  validateRequest,
  iris_notification
);

router.post(
  "/add-shipping-address",
  currentMember,
  requireMember,
  [
    body("city_code").notEmpty(),
    body("city_name").notEmpty(),
    body("contact_address").notEmpty(),
    body("postal_code").notEmpty(),
    body("province_code").notEmpty(),
    body("province_name").notEmpty(),
    body("subdistrict_code").notEmpty(),
    body("subdistrict_name").notEmpty(),
  ],
  validateRequest,
  add_shipping_address
);

router.post(
  "/auth/forget-password",
  [body("email").notEmpty()],
  validateRequest,
  forget_password
);

router.post(
  "/auth/change-password",
  [body("new_password").notEmpty(), body("change_password_token").notEmpty()],
  validateRequest,
  change_password
);

router.post(
  "/support-email",
  [body("subject").notEmpty(), body("body").notEmpty()],
  validateRequest,
  currentMember,
  requireMember,
  send_email_support
);

router.get(
  "/my-transaction-monthly",
  currentMember,
  requireMember,
  my_transaction_monthly
);

router.get("/download-invoice", download_invoice);

export { router as flowRouter };
