import { Router, Request, Response } from "express";
import { advancedResults } from "../middlewares/advanced-result";
import { ProductCategory } from "../models/product_category";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  advancedResults(ProductCategory),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

export { router as productCategoryRouter };
