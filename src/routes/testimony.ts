import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_testimony, update } from "../controller/testimony";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { Testimony } from "../models/testimony";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(Testimony), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("testimony/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [body("name").notEmpty(), body("text").notEmpty()],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("testimony/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [body("name").notEmpty(), body("text").notEmpty()],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_testimony);

export { router as testimonyRouter };
