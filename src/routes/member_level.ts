import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, getById, update } from "../controller/member_level";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";

import { MemberLevel } from "../models/member_level";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(MemberLevel), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("minimum_order").isInt({ min: 0, max: 9999 }).notEmpty(),
    body("hierarchy_number").isInt({ min: 1, max: 999 }).notEmpty(),
    body("commision_percentage").isInt({ min: 0, max: 100 }).notEmpty(),
    body("price_cut_percentage").isInt({ min: 0, max: 100 }).notEmpty(),
  ],
  validateRequest,
  create
);

router.get("/:id", currentAdmin, requireAdmin, getById);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("minimum_order").isInt({ min: 0, max: 9999 }).notEmpty(),
    body("hierarchy_number").isInt({ min: 1, max: 999 }).notEmpty(),
    body("commision_percentage").isInt({ min: 0, max: 100 }).notEmpty(),
    // body("price_cut_percentage").isInt({ min: 0, max: 100 }).notEmpty(),
  ],
  validateRequest,
  update
);

export { router as memberLevelRouter };
