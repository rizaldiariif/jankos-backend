import { Router, Request, Response } from "express";
import { body } from "express-validator";
import {
  create,
  delete_marketing_material,
  update,
} from "../controller/marketing_material";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { MarketingMaterial } from "../models/marketing_material";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  advancedResults(MarketingMaterial),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("marketing-material/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("title").notEmpty(),
    body("text").notEmpty(),
    body("suggestion_date").notEmpty(),
    body("code").notEmpty(),
  ],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("marketing-material/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("title").notEmpty(),
    body("text").notEmpty(),
    body("suggestion_date").notEmpty(),
    body("code").notEmpty(),
  ],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_marketing_material);

export { router as marketingMaterialRouter };
