import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_product_image } from "../controller/product_image";
import { uploader } from "../helpers/api/cloudinary";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";

const router = Router();

// UNIVERSAL GETTER
router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("product_image/images").fields([{ name: "image", maxCount: 1 }]),
  [body("product_id").notEmpty()],
  validateRequest,
  create
);

router.delete("/:id", currentAdmin, requireAdmin, delete_product_image);

export { router as productImageRouter };
