import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_banner_home } from "../controller/banner_home";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { BannerHome } from "../models/banner_home";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(BannerHome), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("testimony/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  validateRequest,
  create
);

router.delete("/:id", currentAdmin, requireAdmin, delete_banner_home);

export { router as bannerHomeRouter };
