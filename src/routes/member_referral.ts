import { Router, Request, Response } from "express";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { MemberReferral } from "../models/member_referral";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  currentAdmin,
  requireAdmin,
  advancedResults(MemberReferral, ["upline_member", "downline_member"]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

export { router as memberReferralRouter };
