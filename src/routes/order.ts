import { Router, Request, Response } from "express";
import { BadRequestError } from "../errors/bad-request-error";
import { advancedResults } from "../middlewares/advanced-result";
import { advancedResultsDesc } from "../middlewares/advanced-result-desc";
import { currentAdmin } from "../middlewares/current-admin";
import { currentMember } from "../middlewares/current-member";
import { requireAdmin } from "../middlewares/require-admin";
import { requireMember } from "../middlewares/require-member";
import { Order } from "../models/order";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  currentAdmin,
  requireAdmin,
  advancedResultsDesc(Order, [
    { association: "order_details", include: ["product"] },
    { association: "order_status_histories", include: ["order_status"] },
    "order_status",
    "member",
  ]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

router.get(
  "/by-id/:id",
  currentAdmin,
  requireAdmin,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const order = await Order.findByPk(id, {
      include: [
        { association: "order_details", include: ["product"] },
        { association: "order_status_histories", include: ["order_status"] },
        "order_status",
        "member",
      ],
    });

    if (!order) {
      throw new BadRequestError("Order with this id is not found!");
    }

    res.status(200).json(order);
  }
);

// UNIVERSAL GETTER FOR EVERY MEMBER
router.get(
  "/by-member",
  currentMember,
  requireMember,
  advancedResults(Order, [
    { association: "order_details", include: ["product"] },
    { association: "order_status_histories", include: ["order_status"] },
    "order_status",
    "member",
  ]),
  (req: Request, res: Response) => {
    const { currentMember } = req;
    const member_id = req.query["member_id.eq"];

    if (!member_id || !currentMember) {
      throw new BadRequestError("Input is not completed!");
    }

    if (typeof member_id !== "string") {
      throw new BadRequestError("Member ID is not supplied!");
    }

    if (member_id != currentMember.id) {
      throw new BadRequestError("Member ID is not match!");
    }

    res.json(res.advancedResults);
  }
);

export { router as orderRouter };
