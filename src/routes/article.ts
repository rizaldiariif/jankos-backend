import { Router, Request, Response } from "express";
import { body } from "express-validator";
import {
  create,
  delete_article,
  get_detail,
  get_recent_articles,
  listing_slugs,
  update,
} from "../controller/article";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { Article } from "../models/article";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(Article), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("article/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("title").notEmpty(),
    body("text").notEmpty(),
    body("author").notEmpty(),
  ],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("article/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [
    body("title").notEmpty(),
    body("text").notEmpty(),
    body("author").notEmpty(),
  ],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_article);

router.get("/listing-slugs", listing_slugs);
router.get("/recent", get_recent_articles);
router.get("/:slug", get_detail);

export { router as articleRouter };
