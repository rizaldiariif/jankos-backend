import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_admin_level, update } from "../controller/admin_level";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { AdminLevel } from "../models/admin_level";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(AdminLevel), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("hierarchy_number").isInt({ min: 2, max: 998 }).notEmpty(),
  ],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  [
    body("name").notEmpty(),
    body("hierarchy_number").isInt({ min: 2, max: 998 }).notEmpty(),
  ],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_admin_level);

export { router as adminLevelRouter };
