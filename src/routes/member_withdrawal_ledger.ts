import { Request, Response, Router } from "express";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { MemberWithdrawalLedger } from "../models/member_withdrawal_ledger";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  currentAdmin,
  requireAdmin,
  advancedResults(MemberWithdrawalLedger, ["member"]),
  (_req: Request, res: Response ) => res.json(res.advancedResults)
);

export { router as memberWithdrawalLedgerRouter };
