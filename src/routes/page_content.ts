import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { update } from "../controller/page_content";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { PageContent } from "../models/page_content";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(PageContent), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

// router.post(
//   "/",
//   currentAdmin,
//   requireAdmin,
//   uploader("page_content/files").fields([
//     { name: "content_file", maxCount: 1 },
//   ]),
//   [body("name").notEmpty(), body("text").notEmpty()],
//   validateRequest,
//   create
// );

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("page_content/files").fields([
    { name: "content_file", maxCount: 1 },
  ]),
  [body("content").notEmpty()],
  validateRequest,
  update
);

export { router as pageContentRouter };
