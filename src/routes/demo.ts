import axios from "axios";
import { Request, Response, Router } from "express";
import { Op } from "sequelize";
import { current_member_flow, signup_flow, sync } from "../controller/demo";
import { BadRequestError } from "../errors/bad-request-error";
import { Order } from "../models/order";
// @ts-ignore
import { combineLoop } from "box-dimension-calculator";

const router = Router();

router.get("/signup-flow", signup_flow);
router.get("/current-member-flow", current_member_flow);

router.get("/test", async (req: Request, res: Response) => {
  const demo_client = axios.create({ baseURL: process.env.APP_HOST });

  try {
    const response_1 = await demo_client.post("/api/admins/auth/signin", {
      email: "superadmin@gmail.com",
      password: "ABCabc123_",
    });

    const order = await Order.findOne({
      where: {
        shipping_id: {
          [Op.ne]: null,
        },
      },
    });

    if (!order) {
      throw new BadRequestError("Order not found!");
    }

    const response_2 = await demo_client.get(
      `/api/flow/shipping-label/${order.id}`,
      {
        headers: {
          Authorization: `Bearer ${response_1.data.jwt}`,
        },
      }
    );

    res
      .status(200)
      .set("Content-Type", `text/vcard; name="${order.id}.pdf"`)
      .set("Content-Disposition", `inline; filename="${order.id}.pdf"`)
      .send(response_2.data);
  } catch (error) {
    console.log(error);
    res.status(500).json("error hehe");
  }
});

router.get("/test-2", async (req: Request, res: Response) => {
  const item_dimensions: {
    l: number;
    w: number;
    h: number;
  }[] = [];

  for (let index = 0; index < 10; index++) {
    item_dimensions.push({
      l: 5,
      w: 5,
      h: 5,
    });
  }

  const shipping_dimension = combineLoop(item_dimensions);

  res.status(200).json(shipping_dimension);
});

router.get("/sync", sync);

export { router as demoRouter };
