import { Router, Request, Response } from "express";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { MemberReferralLedger } from "../models/member_referral_ledger";

const router = Router();

// UNIVERSAL GETTER
router.get(
  "/",
  currentAdmin,
  requireAdmin,
  advancedResults(MemberReferralLedger, [
    {
      association: "member_referral",
      include: ["upline_member", "referred_member"],
    },
    "order",
  ]),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);

export { router as memberReferralLedgerRouter };
