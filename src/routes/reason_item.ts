import { Router, Request, Response } from "express";
import { body } from "express-validator";
import { create, delete_reason, update } from "../controller/reason_item";
import { uploader } from "../helpers/api/cloudinary";
import { advancedResults } from "../middlewares/advanced-result";
import { currentAdmin } from "../middlewares/current-admin";
import { requireAdmin } from "../middlewares/require-admin";
import { validateRequest } from "../middlewares/validate-request";
import { ReasonItem } from "../models/reason_item";

const router = Router();

// UNIVERSAL GETTER
router.get("/", advancedResults(ReasonItem), (_req: Request, res: Response) =>
  res.json(res.advancedResults)
);

router.post(
  "/",
  currentAdmin,
  requireAdmin,
  uploader("reason-item/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [body("title").notEmpty(), body("text").notEmpty()],
  validateRequest,
  create
);

router.post(
  "/:id",
  currentAdmin,
  requireAdmin,
  uploader("testimony/thumbnails").fields([
    { name: "image_thumbnail", maxCount: 1 },
  ]),
  [body("title").notEmpty(), body("text").notEmpty()],
  validateRequest,
  update
);

router.delete("/:id", currentAdmin, requireAdmin, delete_reason);

export { router as reasonItemRouter };
