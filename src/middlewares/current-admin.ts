import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

import { Admin } from "../models/admin";

interface AdminPayload {
  id: string;
  email: string;
}

declare global {
  namespace Express {
    interface Request {
      currentAdmin?: Admin;
    }
  }
}

export const currentAdmin = async (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return next();
  }

  try {
    const [_bearer, token] = req.headers.authorization.split(" ");

    const payload = jwt.verify(token, process.env.APP_JWT_KEY!) as AdminPayload;

    const existingAdmin = await Admin.findOne({
      where: {
        id: payload.id,
        email: payload.email,
      },
      include: ["admin_level"],
    });

    if (existingAdmin) {
      req.currentAdmin = existingAdmin;
    }
  } catch (error) {}

  next();
};
