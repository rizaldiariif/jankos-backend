import { Request, Response, NextFunction } from "express";
import { NotAuthorizedError } from "../errors/not-authorized-error";

export const requireMember = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (!req.currentMember) {
    throw new NotAuthorizedError();
  }

  next();
};
