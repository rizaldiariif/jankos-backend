import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

import { Member } from "../models/member";
import { MemberLevel } from "../models/member_level";
import { MemberReferral } from "../models/member_referral";

interface MemberPayload {
  id: string;
  email: string;
}

declare global {
  namespace Express {
    interface Request {
      currentMember?: Member;
    }
  }
}

export const currentMember = async (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return next();
  }

  try {
    const [_bearer, token] = req.headers.authorization.split(" ");

    const payload = jwt.verify(
      token,
      process.env.APP_JWT_KEY!
    ) as MemberPayload;

    const existing_member = await Member.findOne({
      where: {
        id: payload.id,
        email: payload.email,
      },
      include: [
        "member_level",
        {
          model: MemberReferral,
          include: [
            {
              association: "member_referral_ledgers",
              include: [{ association: "order", include: ["order_details"] }],
            },
            { association: "referred_member", include: ["member_level"] },
          ],
          as: "member_referrals",
        },
        "member_point_ledgers",
        "member_bank_account",
      ],
    });

    if (existing_member) {
      req.currentMember = existing_member;
    }
  } catch (error) {}

  next();
};
