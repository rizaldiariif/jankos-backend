import { Request, Response, NextFunction } from "express";
import {
  ModelDefined,
  Model,
  Order,
  WhereOptions,
  IncludeOptions,
  ModelType,
  FindAttributeOptions,
  Op,
  ModelStatic,
  Sequelize,
} from "sequelize";
import { db } from "../config/db";

interface Pagination {
  next?: {
    page: number;
    limit: number;
  } | null;
  prev?: {
    page: number;
    limit: number;
  } | null;
}

declare global {
  namespace Express {
    interface Response {
      advancedResults?: {
        pagination: Pagination;
        total_data: number;
        count: number;
        data: Model[];
      };
    }
  }
}

export const advancedResultsDesc =
  (model_type: ModelType, populate?: any[]) =>
  async (req: Request, res: Response, next: NextFunction) => {
    const model = db.model(model_type.name);

    // Copy req.query
    const reqQuery = { ...req.query };

    // Initialize Filtering Conditions
    const whereConditions: WhereOptions = {};
    let selectConditions: FindAttributeOptions | undefined = [];
    const sortConditions: Order = [["createdAt", "DESC"]];

    // Pagination
    let page = 1;
    let limit = 100;
    if (req.query.page && typeof req.query.page === "string") {
      page = parseInt(req.query.page, 10);
    }
    if (req.query.limit && typeof req.query.limit === "string") {
      limit = parseInt(req.query.limit, 10);
    }
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    // Fields to exclude
    const removeFields = ["select", "sort", "page", "limit"];

    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach((param) => delete reqQuery[param]);

    // Where Conditions
    for (let [query_key, query_value] of Object.entries(reqQuery)) {
      const [attribute_name, attribute_modifier] = query_key.split(".");

      if (typeof whereConditions[attribute_name] !== "object") {
        whereConditions[attribute_name] = {};
      }

      if (typeof query_value !== "string") {
        throw new Error("test");
      }

      if (
        ["or", "between", "notBetween", "in", "notIn"].includes(
          attribute_modifier
        )
      ) {
        query_value = query_value.split(",");
      }

      // @ts-ignore
      whereConditions[attribute_name][Op[attribute_modifier]] = query_value;
    }

    // Select Conditions
    if (req.query.select && typeof req.query.select === "string") {
      selectConditions = [...req.query.select.split(",")];
    } else {
      selectConditions = undefined;
    }

    // Finding resource
    const results = await model.findAll({
      where: whereConditions,
      attributes: selectConditions,
      order: [["createdAt", "DESC"]],
      include: populate,
      limit: limit,
      offset: startIndex,
    });
    const total = await model.count({
      where: whereConditions,
    });

    // Select Fields

    // Sort
    // if (req.query.sort && typeof req.query.sort === "string") {
    //   const sortBy = req.query.sort.split(",").join(" ");
    //   query = query.sort(sortBy);
    // } else {
    //   query = query.sort("-createdAt");
    // }

    // query = query.skip(startIndex).limit(limit);

    // if (populate) {
    //   query = query.populate(populate);
    // }

    // Executing query
    // const results = await query;

    // Pagination result
    let pagination: Pagination = {
      prev: null,
      next: null,
    };

    if (endIndex < total) {
      pagination.next = {
        page: page + 1,
        limit,
      };
    }

    if (startIndex > 0) {
      pagination.prev = {
        page: page - 1,
        limit,
      };
    }

    res.advancedResults = {
      count: results.length,
      total_data: total,
      pagination,
      data: results,
    };

    next();
  };
