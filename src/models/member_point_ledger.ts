import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";
import { MemberReferral } from "./member_referral";
import { Order } from "./order";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberPointLedgerAttributes {
  id: string;
  member_id: string;
  order_id: string;
  amount: number;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberPointLedgerCreationAttributes
  extends Optional<
    MemberPointLedgerAttributes,
    "id" | "createdAt" | "updatedAt"
  > {}

class MemberPointLedger
  extends Model<
    MemberPointLedgerAttributes,
    MemberPointLedgerCreationAttributes
  >
  implements MemberPointLedgerAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public order_id!: string;
  public amount!: number;

  // Associations
  public getMember!: BelongsToGetAssociationMixin<Member>;
  public readonly member?: Member;

  public static associations: {
    member: Association<Member, MemberPointLedger>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberPointLedger.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_point_ledger",
    sequelize: db,
  }
);

export { MemberPointLedger };
