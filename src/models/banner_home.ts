import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface BannerHomeAttributes {
  id: string;
  thumbnail: string;
  createdAt: Date;
  updatedAt: Date;
}

interface BannerHomeCreationAttributes
  extends Optional<BannerHomeAttributes, "id" | "createdAt" | "updatedAt"> {}

class BannerHome
  extends Model<BannerHomeAttributes, BannerHomeCreationAttributes>
  implements BannerHomeAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public thumbnail!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

BannerHome.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "banner_home",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { BannerHome };
