import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  HasOneGetAssociationMixin,
  HasOneSetAssociationMixin,
  Model,
  Optional,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  HasOneCreateAssociationMixin,
} from "sequelize";
import { db } from "../config/db";
import { PasswordClient } from "../helpers/function/password-client";
import { MemberBankAccount } from "./member_bank_account";
import { MemberLevel } from "./member_level";
import { MemberLevelHistory } from "./member_level_history";
import { MemberPointLedger } from "./member_point_ledger";
import { MemberReferral } from "./member_referral";
import { MemberShippingAddress } from "./member_shipping_address";
import { MemberWithdrawalLedger } from "./member_withdrawal_ledger";
import { Order } from "./order";

const PROTECTED_ATTRIBUTES = ["password"];

export interface MemberAttributes {
  id: string;
  name: string;
  email: string;
  phone: string;
  address: string;
  member_level_id: string;
  referral_code: string;
  // image_profile: string;
  image_id_card: string;
  status_verification: boolean;
  social_media_url: string;
  marketplace_url: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberCreationAttributes
  extends Optional<
    MemberAttributes,
    | "id"
    | "referral_code"
    | "status_verification"
    | "member_level_id"
    | "social_media_url"
    | "marketplace_url"
    | "createdAt"
    | "updatedAt"
  > {}

class Member
  extends Model<MemberAttributes, MemberCreationAttributes>
  implements MemberAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public email!: string;
  public phone!: string;
  public address!: string;
  public referral_code!: string;
  public member_level_id!: string;
  // public image_profile!: string;
  public social_media_url!: string;
  public marketplace_url!: string;
  public image_id_card!: string;
  public status_verification!: boolean;
  public password!: string;

  // Associations
  public getMemberShippingAddresses!: HasManyGetAssociationsMixin<MemberShippingAddress>; // Note the null assertions!
  public addMemberShippingAddress!: HasManyAddAssociationMixin<
    MemberShippingAddress,
    string
  >;
  public hasMemberShippingAddress!: HasManyHasAssociationMixin<
    MemberShippingAddress,
    string
  >;
  public countMemberShippingAddress!: HasManyCountAssociationsMixin;
  public createMemberShippingAddress!: HasManyCreateAssociationMixin<MemberShippingAddress>;
  public readonly member_shipping_addresses?: MemberShippingAddress[];

  public getMemberLevel!: BelongsToGetAssociationMixin<MemberLevel>;
  public setMemberLevel!: BelongsToSetAssociationMixin<MemberLevel, string>;
  public readonly member_level?: MemberLevel;

  public getMemberLevelHistory!: HasManyGetAssociationsMixin<MemberLevelHistory>; // Note the null assertions!
  public addMemberLevelHistory!: HasManyAddAssociationMixin<
    MemberLevelHistory,
    string
  >;
  public hasMemberLevelHistory!: HasManyHasAssociationMixin<
    MemberLevelHistory,
    string
  >;
  public countMemberLevelHistory!: HasManyCountAssociationsMixin;
  public createMemberLevelHistory!: HasManyCreateAssociationMixin<MemberLevelHistory>;
  public readonly member_level_histories?: MemberLevelHistory[];

  public getMemberBankAccount!: HasOneGetAssociationMixin<MemberBankAccount>;
  public createMemberBankAccount!: HasOneCreateAssociationMixin<MemberBankAccount>;
  public setMemberBankAccount!: HasOneSetAssociationMixin<
    MemberBankAccount,
    string
  >;
  public readonly member_bank_account?: MemberBankAccount;

  public getOrders!: HasManyGetAssociationsMixin<Order>;
  public addOrder!: HasManyAddAssociationMixin<Order, string>;
  public hasOrder!: HasManyHasAssociationMixin<Order, string>;
  public countOrder!: HasManyCountAssociationsMixin;
  public createOrder!: HasManyCreateAssociationMixin<Order>;
  public readonly orders?: Order[];

  public static associations: {
    member_shipping_addresses: Association<Member, MemberShippingAddress>;
    member_level: Association<MemberLevel, Member>;
    member_level_histories: Association<Member, MemberLevelHistory>;
    member_bank_account: Association<Member, MemberBankAccount>;
    orders: Association<Member, Order>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Member.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    email: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    phone: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    address: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    referral_code: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    // image_profile: {
    //   type: DataTypes.STRING,
    //   allowNull: false,
    // },
    image_id_card: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status_verification: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    password: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    member_level_id: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    social_media_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    marketplace_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member",
    sequelize: db,
  }
);

// HOOKS
Member.beforeValidate(async (member, _options) => {
  if (!member.referral_code) {
    member.referral_code = `JANKOSGLOW-${Math.ceil(Math.random() * 10000) + 1}`;
  }
});

Member.beforeSave(async (member, _options) => {
  if (member.changed("password")) {
    const hashed = await PasswordClient.toHash(member.password);
    member.password = hashed;
  }
});

Member.afterSave(async (member, _options) => {
  if (member.changed("member_level_id") && member.member_level_id) {
    await MemberLevelHistory.create({
      member_id: member.id,
      member_level_id: member.member_level_id,
    });
  }
});

// ASSOCIATIONS
Member.hasMany(MemberShippingAddress, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "member_shipping_addresses",
});
MemberShippingAddress.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

Member.hasMany(MemberLevelHistory, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "member_level_histories",
});
MemberLevelHistory.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

Member.hasOne(MemberBankAccount, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "member_bank_account",
});
MemberBankAccount.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

Member.hasMany(MemberReferral, {
  sourceKey: "id",
  foreignKey: "upline_member_id",
  as: "member_referrals",
});
MemberReferral.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "upline_member_id",
  as: "upline_member",
});
MemberReferral.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "referred_member_id",
  as: "referred_member",
});

Member.hasMany(Order, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "orders",
});
Order.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

MemberLevel.hasMany(Member, {
  sourceKey: "id",
  foreignKey: "member_level_id",
  as: "members",
});
Member.belongsTo(MemberLevel, {
  targetKey: "id",
  foreignKey: "member_level_id",
  as: "member_level",
});

Member.hasMany(MemberPointLedger, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "member_point_ledgers",
});
MemberPointLedger.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

Member.hasMany(MemberWithdrawalLedger, {
  sourceKey: "id",
  foreignKey: "member_id",
  as: "member_withdrawal_ledgers",
});
MemberWithdrawalLedger.belongsTo(Member, {
  targetKey: "id",
  foreignKey: "member_id",
  as: "member",
});

export { Member };
