import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";
import { MemberLevel } from "./member_level";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberLevelHistoryAttributes {
  id: string;
  member_id: string;
  member_level_id: string;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberLevelHistoryCreationAttributes
  extends Optional<
    MemberLevelHistoryAttributes,
    "id" | "createdAt" | "updatedAt"
  > {}

class MemberLevelHistory
  extends Model<
    MemberLevelHistoryAttributes,
    MemberLevelHistoryCreationAttributes
  >
  implements MemberLevelHistoryAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public member_level_id!: string;

  // Associations
  public getMember!: BelongsToGetAssociationMixin<Member>;
  public readonly member?: Member;

  public getMemberLevel!: BelongsToGetAssociationMixin<MemberLevel>;
  public readonly member_level?: MemberLevel;

  public static associations: {
    member: Association<Member, MemberLevelHistory>;
    member_level: Association<MemberLevel, Member>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberLevelHistory.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    member_level_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: MemberLevel,
        key: "id",
      },
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_level_history",
    sequelize: db,
  }
);

// ASSOCIATIONS
MemberLevel.hasMany(MemberLevelHistory, {
  sourceKey: "id",
  foreignKey: "member_level_id",
  as: "member_level_histories",
});
MemberLevelHistory.belongsTo(MemberLevel, {
  targetKey: "id",
  foreignKey: "member_level_id",
  as: "member_level",
});

export { MemberLevelHistory };
