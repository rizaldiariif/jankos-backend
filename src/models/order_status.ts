import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Order } from "./order";
import { OrderStatusHistory } from "./order_status_history";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface OrderStatusAttributes {
  id: string;
  code: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface OrderStatusCreationAttributes
  extends Optional<
    OrderStatusAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class OrderStatus
  extends Model<OrderStatusAttributes, OrderStatusCreationAttributes>
  implements OrderStatusAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public code!: string;
  public name!: string;
  public receiver_name!: string;
  public receiver_phone!: string;

  // Associations
  public getOrders!: HasManyGetAssociationsMixin<Order>;
  public addOrder!: HasManyAddAssociationMixin<Order, string>;
  public hasOrder!: HasManyHasAssociationMixin<Order, string>;
  public countOrder!: HasManyCountAssociationsMixin;
  public createOrder!: HasManyCreateAssociationMixin<Order>;
  public readonly orders?: Order[];

  public getOrderStatusHistories!: HasManyGetAssociationsMixin<OrderStatusHistory>;
  public addOrderStatusHistory!: HasManyAddAssociationMixin<
    OrderStatusHistory,
    string
  >;
  public hasOrderStatusHistory!: HasManyHasAssociationMixin<
    OrderStatusHistory,
    string
  >;
  public countOrderStatusHistory!: HasManyCountAssociationsMixin;
  public createOrderStatusHistory!: HasManyCreateAssociationMixin<OrderStatusHistory>;
  public readonly order_status_histories?: OrderStatusHistory[];

  public static associations: {
    orders: Association<OrderStatus, Order>;
    order_status_histories: Association<Order, OrderStatusHistory>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

OrderStatus.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "order_status",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS

// ASSOCIATIONS

OrderStatus.hasMany(OrderStatusHistory, {
  sourceKey: "id",
  foreignKey: "order_status_id",
  as: "order_status_histories",
});
OrderStatusHistory.belongsTo(OrderStatus, {
  targetKey: "id",
  foreignKey: "order_status_id",
  as: "order_status",
});

export { OrderStatus };
