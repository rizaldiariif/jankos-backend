import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberWithdrawalLedgerAttributes {
  id: string;
  member_id: string;
  reference_no: string;
  amount: number;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberWithdrawalLedgerCreationAttributes
  extends Optional<
    MemberWithdrawalLedgerAttributes,
    "id" | "status" | "reference_no" | "createdAt" | "updatedAt"
  > {}

class MemberWithdrawalLedger
  extends Model<
    MemberWithdrawalLedgerAttributes,
    MemberWithdrawalLedgerCreationAttributes
  >
  implements MemberWithdrawalLedgerAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public reference_no!: string;
  public amount!: number;
  public status!: string;

  // Associations
  public getMember!: BelongsToGetAssociationMixin<Member>;
  public readonly member?: Member;

  public static associations: {
    member: Association<Member, MemberWithdrawalLedger>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberWithdrawalLedger.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    reference_no: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_withdrawal_ledger",
    sequelize: db,
  }
);

export { MemberWithdrawalLedger };
