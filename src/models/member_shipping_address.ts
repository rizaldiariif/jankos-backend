import {
  DataTypes,
  Model,
  Optional,
  Association,
  BelongsToGetAssociationMixin,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberShippingAddressAttributes {
  id: string;
  member_id: string;
  address_name: string;
  contact_name: string;
  contact_phone: string;
  contact_address: string;
  contact_email: string;
  province_code: string;
  province_name: string;
  city_code: string;
  city_name: string;
  subdistrict_code: string;
  subdistrict_name: string;
  postal_code: string;
  latitude: string;
  longitude: string;
  note: string;
  primary_address: boolean;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberShippingAddressCreationAttributes
  extends Optional<
    MemberShippingAddressAttributes,
    | "id"
    | "note"
    | "primary_address"
    | "createdAt"
    | "updatedAt"
    | "latitude"
    | "longitude"
  > {}

class MemberShippingAddress
  extends Model<
    MemberShippingAddressAttributes,
    MemberShippingAddressCreationAttributes
  >
  implements MemberShippingAddressAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public address_name!: string;
  public contact_name!: string;
  public contact_phone!: string;
  public contact_address!: string;
  public contact_email!: string;
  public province_code!: string;
  public province_name!: string;
  public city_code!: string;
  public city_name!: string;
  public subdistrict_code!: string;
  public subdistrict_name!: string;
  public postal_code!: string;
  public latitude!: string;
  public longitude!: string;
  public note!: string;
  public primary_address!: boolean;

  // Associations
  public getMember!: BelongsToGetAssociationMixin<Member>; // Note the null assertions!
  public readonly member?: Member;

  public static associations: {
    member: Association<Member, MemberShippingAddress>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberShippingAddress.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    address_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    province_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    province_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subdistrict_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subdistrict_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    postal_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    latitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    note: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    primary_address: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_shipping_address",
    sequelize: db,
  }
);

export { MemberShippingAddress };
