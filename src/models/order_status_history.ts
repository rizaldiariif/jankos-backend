import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Order } from "./order";
import { OrderStatus } from "./order_status";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface OrderStatusHistoryAttributes {
  id: string;
  order_id: string;
  order_status_id: string;
  note: string;
  createdAt: Date;
  updatedAt: Date;
}

interface OrderStatusHistoryCreationAttributes
  extends Optional<
    OrderStatusHistoryAttributes,
    "id" | "note" | "createdAt" | "updatedAt"
  > {}

class OrderStatusHistory
  extends Model<
    OrderStatusHistoryAttributes,
    OrderStatusHistoryCreationAttributes
  >
  implements OrderStatusHistoryAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public order_id!: string;
  public order_status_id!: string;
  public note!: string;

  // Associations
  public getOrder!: BelongsToGetAssociationMixin<Order>;
  public setOrder!: BelongsToSetAssociationMixin<Order, string>;
  public readonly order?: Order;

  public getOrderStatus!: BelongsToGetAssociationMixin<OrderStatus>;
  public setOrderStatus!: BelongsToSetAssociationMixin<OrderStatus, string>;
  public readonly order_status?: OrderStatus;

  public static associations: {
    order: Association<OrderStatusHistory, Order>;
    order_status: Association<OrderStatusHistory, OrderStatus>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

OrderStatusHistory.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    order_status_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    note: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "order_status_history",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS

export { OrderStatusHistory };
