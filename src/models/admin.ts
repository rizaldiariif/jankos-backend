import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { PasswordClient } from "../helpers/function/password-client";
import { AdminLevel } from "./admin_level";

const PROTECTED_ATTRIBUTES = ["password"];

export interface AdminAttributes {
  id: string;
  name: string;
  email: string;
  password: string;
  admin_level_id: string;
  createdAt: Date;
  updatedAt: Date;
}

interface AdminCreationAttributes
  extends Optional<AdminAttributes, "id" | "createdAt" | "updatedAt"> {}

class Admin
  extends Model<AdminAttributes, AdminCreationAttributes>
  implements AdminAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public email!: string;
  public phone!: string;
  public password!: string;
  public admin_level_id!: string;

  // Associations
  public getAdminLevel!: BelongsToGetAssociationMixin<AdminLevel>;
  public setAdminLevel!: BelongsToSetAssociationMixin<AdminLevel, string>;
  public readonly admin_level?: AdminLevel;

  public static associations: {
    admin_level: Association<AdminLevel, Admin>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Admin.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    email: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    password: {
      type: new DataTypes.STRING(),
      allowNull: false,
    },
    admin_level_id: {
      type: new DataTypes.UUID(),
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "admin",
    sequelize: db,
  }
);

// HOOKS
Admin.beforeSave(async (member, _options) => {
  if (member.changed("password")) {
    const hashed = await PasswordClient.toHash(member.password);
    member.password = hashed;
  }
});

// ASSOCIATIONS
AdminLevel.hasMany(Admin, {
  sourceKey: "id",
  foreignKey: "admin_level_id",
  as: "admins",
});
Admin.belongsTo(AdminLevel, {
  targetKey: "id",
  foreignKey: "admin_level_id",
  as: "admin_level",
});

export { Admin };
