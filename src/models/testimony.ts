import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface TestimonyAttributes {
  id: string;
  name: string;
  thumbnail: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
}

interface TestimonyCreationAttributes
  extends Optional<TestimonyAttributes, "id" | "createdAt" | "updatedAt"> {}

class Testimony
  extends Model<TestimonyAttributes, TestimonyCreationAttributes>
  implements TestimonyAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public thumbnail!: string;
  public text!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Testimony.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "testimony",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { Testimony };
