import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";
import { MemberReferralLedger } from "./member_referral_ledger";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberReferralAttributes {
  id: string;
  upline_member_id: string;
  referred_member_id: string;
  referral_code: string;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberReferralCreationAttributes
  extends Optional<
    MemberReferralAttributes,
    "id" | "createdAt" | "updatedAt"
  > {}

class MemberReferral
  extends Model<MemberReferralAttributes, MemberReferralCreationAttributes>
  implements MemberReferralAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public upline_member_id!: string;
  public referred_member_id!: string;
  public referral_code!: string;

  // Associations
  public getUplineMember!: BelongsToGetAssociationMixin<Member>;
  public readonly upline_member?: Member;

  public getReferredMember!: BelongsToGetAssociationMixin<Member>;
  public readonly referred_member?: Member;

  public getMemberReferralLedgers!: HasManyGetAssociationsMixin<MemberReferralLedger>;
  public addMemberReferralLedger!: HasManyAddAssociationMixin<
    MemberReferralLedger,
    string
  >;
  public hasMemberReferralLedger!: HasManyHasAssociationMixin<
    MemberReferralLedger,
    string
  >;
  public countMemberReferralLedger!: HasManyCountAssociationsMixin;
  public createMemberReferralLedger!: HasManyCreateAssociationMixin<MemberReferralLedger>;
  public readonly member_referral_ledgers?: MemberReferralLedger[];

  public static associations: {
    upline_member: Association<Member, MemberReferral>;
    referred_member: Association<Member, MemberReferral>;
    member_referral_ledgers: Association<Member, MemberReferralLedger>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberReferral.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    upline_member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    referred_member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    referral_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_referral",
    sequelize: db,
  }
);

// ASSOCIATIONS
MemberReferral.hasMany(MemberReferralLedger, {
  sourceKey: "id",
  foreignKey: "member_referral_id",
  as: "member_referral_ledgers",
});
MemberReferralLedger.belongsTo(MemberReferral, {
  targetKey: "id",
  foreignKey: "member_referral_id",
  as: "member_referral",
});

export { MemberReferral };
