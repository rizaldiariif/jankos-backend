import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";
import { MemberReferralLedger } from "./member_referral_ledger";
import { OrderDetail } from "./order_detail";
import { OrderStatus } from "./order_status";
import { OrderStatusHistory } from "./order_status_history";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface OrderAttributes {
  id: string;
  member_id: string;
  payment_url: string;
  contact_name: string;
  contact_phone: string;
  contact_address: string;
  contact_email: string;
  province_code: string;
  province_name: string;
  city_code: string;
  city_name: string;
  subdistrict_code: string;
  subdistrict_name: string;
  latitude: string;
  longitude: string;
  postal_code: string;
  address_note: string;
  shipping_type: string;
  shipping_courier_code: string;
  shipping_courier_service: string;
  shipping_price: number;
  shipping_id: string;
  airwaybill_number: string;
  order_status_id: string;
  increment_count: number;
  createdAt: Date;
  updatedAt: Date;
}

interface OrderCreationAttributes
  extends Optional<
    OrderAttributes,
    | "id"
    | "address_note"
    | "payment_url"
    | "shipping_id"
    | "airwaybill_number"
    | "createdAt"
    | "updatedAt"
    | "latitude"
    | "longitude"
    | "shipping_courier_code"
    | "shipping_courier_service"
    | "increment_count"
  > {}

class Order
  extends Model<OrderAttributes, OrderCreationAttributes>
  implements OrderAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public payment_url!: string;
  public contact_name!: string;
  public contact_phone!: string;
  public contact_address!: string;
  public contact_email!: string;
  public province_code!: string;
  public province_name!: string;
  public city_code!: string;
  public city_name!: string;
  public subdistrict_code!: string;
  public subdistrict_name!: string;
  public latitude!: string;
  public longitude!: string;
  public postal_code!: string;
  public address_note!: string;
  public shipping_type!: string;
  public shipping_courier_code!: string;
  public shipping_courier_service!: string;
  public shipping_price!: number;
  public shipping_id!: string;
  public airwaybill_number!: string;
  public order_status_id!: string;
  public increment_count!: number;

  // Associations
  public getOrderStatus!: BelongsToGetAssociationMixin<OrderStatus>;
  public setOrderStatus!: BelongsToSetAssociationMixin<OrderStatus, string>;
  public readonly order_status?: OrderStatus;

  public getMember!: BelongsToGetAssociationMixin<Member>;
  public setMember!: BelongsToSetAssociationMixin<Member, string>;
  public readonly member?: Member;

  public getOrderDetails!: HasManyGetAssociationsMixin<OrderDetail>;
  public addOrderDetail!: HasManyAddAssociationMixin<OrderDetail, string>;
  public hasOrderDetail!: HasManyHasAssociationMixin<OrderDetail, string>;
  public countOrderDetail!: HasManyCountAssociationsMixin;
  public createOrderDetail!: HasManyCreateAssociationMixin<OrderDetail>;
  public readonly order_details?: OrderDetail[];

  public getOrderStatusHistories!: HasManyGetAssociationsMixin<OrderStatusHistory>;
  public addOrderStatusHistory!: HasManyAddAssociationMixin<
    OrderStatusHistory,
    string
  >;
  public hasOrderStatusHistory!: HasManyHasAssociationMixin<
    OrderStatusHistory,
    string
  >;
  public countOrderStatusHistory!: HasManyCountAssociationsMixin;
  public createOrderStatusHistory!: HasManyCreateAssociationMixin<OrderStatusHistory>;
  public readonly order_status_histories?: OrderStatusHistory[];

  public static associations: {
    order_status: Association<Order, OrderStatus>;
    member: Association<Order, Member>;
    order_details: Association<Order, OrderDetail>;
    order_status_histories: Association<Order, OrderStatusHistory>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Order.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    payment_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    contact_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    province_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    province_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subdistrict_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    subdistrict_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    latitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    postal_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    address_note: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    shipping_type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    shipping_courier_code: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    shipping_courier_service: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    shipping_price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    shipping_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    airwaybill_number: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    order_status_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    increment_count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "order",
    sequelize: db,
  }
);

// HOOKS
Order.afterCreate(async (order, _options) => {
  const total_order = await Order.count();
  order.set("increment_count", total_order);
  await order.save();
});

Order.afterSave(async (order, _options) => {
  if (order.changed("order_status_id") && order.order_status_id) {
    await OrderStatusHistory.create({
      order_id: order.id,
      order_status_id: order.order_status_id,
    });
  }
});

// ASSOCIATIONS
Order.hasMany(OrderDetail, {
  sourceKey: "id",
  foreignKey: "order_id",
  as: "order_details",
});
OrderDetail.belongsTo(Order, {
  targetKey: "id",
  foreignKey: "order_id",
  as: "order",
});

Order.hasMany(OrderStatusHistory, {
  sourceKey: "id",
  foreignKey: "order_id",
  as: "order_status_histories",
});
OrderStatusHistory.belongsTo(Order, {
  targetKey: "id",
  foreignKey: "order_id",
  as: "order",
});

OrderStatus.hasMany(Order, {
  sourceKey: "id",
  foreignKey: "order_status_id",
  as: "orders",
});
Order.belongsTo(OrderStatus, {
  targetKey: "id",
  foreignKey: "order_status_id",
  as: "order_status",
});

Order.hasOne(MemberReferralLedger, {
  sourceKey: "id",
  foreignKey: "order_id",
  as: "member_referral_ledger",
});
MemberReferralLedger.belongsTo(Order, {
  targetKey: "id",
  foreignKey: "order_id",
  as: "order",
});

export { Order };
