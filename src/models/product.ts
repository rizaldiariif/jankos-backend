import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { MemberLevel } from "./member_level";
import { OrderDetail } from "./order_detail";
import { ProductImage } from "./product_image";
import { ProductLevelPrice } from "./produt_level_price";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ProductAttributes {
  id: string;
  product_category_id: string;
  is_starter: boolean;
  is_package: boolean;
  name: string;
  price: number;
  weight: number;
  width: number;
  height: number;
  length: number;
  description: string;
  short_description: string;
  thumbnail: string;
  rating: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface ProductCreationAttributes
  extends Optional<
    ProductAttributes,
    | "id"
    | "is_starter"
    | "is_package"
    | "createdAt"
    | "updatedAt"
    | "deletedAt"
    | "length"
    | "width"
    | "height"
    | "rating"
    | "short_description"
  > {}

class Product
  extends Model<ProductAttributes, ProductCreationAttributes>
  implements ProductAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public is_starter!: boolean;
  public is_package!: boolean;
  public product_category_id!: string;
  public name!: string;
  public price!: number;
  public weight!: number;
  public width!: number;
  public height!: number;
  public length!: number;
  public description!: string;
  public short_description!: string;
  public thumbnail!: string;
  public rating!: number;

  // Associations
  public getOrderDetails!: HasManyGetAssociationsMixin<OrderDetail>;
  public addOrderDetail!: HasManyAddAssociationMixin<OrderDetail, string>;
  public hasOrderDetail!: HasManyHasAssociationMixin<OrderDetail, string>;
  public countOrderDetail!: HasManyCountAssociationsMixin;
  public createOrderDetail!: HasManyCreateAssociationMixin<OrderDetail>;
  public readonly order_details?: OrderDetail[];

  public readonly product_level_prices?: ProductLevelPrice[];

  public static associations: {
    order_details: Association<Product, OrderDetail>;
    product_level_prices: Association<Product, ProductLevelPrice>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

Product.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    product_category_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    is_package: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    is_starter: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    weight: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    short_description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    width: {
      type: DataTypes.FLOAT,
      defaultValue: 1,
    },
    height: {
      type: DataTypes.FLOAT,
      defaultValue: 1,
    },
    length: {
      type: DataTypes.FLOAT,
      defaultValue: 1,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    rating: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "product",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS
Product.afterCreate(async (product, _options) => {
  // Create Level Prices
  const member_levels = await MemberLevel.findAll();
  for (const member_level of member_levels) {
    await ProductLevelPrice.create({
      product_id: product.id,
      member_level_id: member_level.id,
      price: product.price,
    });
  }
});
Product.afterDestroy(async (product, _options) => {
  // Delete respective product level price
  await ProductLevelPrice.destroy({
    where: {
      product_id: product.id,
    },
  });
});

// ASSOCIATIONS
Product.hasMany(OrderDetail, {
  sourceKey: "id",
  foreignKey: "product_id",
  as: "order_details",
});
OrderDetail.belongsTo(Product, {
  targetKey: "id",
  foreignKey: "product_id",
  as: "product",
});

Product.hasMany(ProductLevelPrice, {
  sourceKey: "id",
  foreignKey: "product_id",
  as: "product_level_prices",
});
ProductLevelPrice.belongsTo(Product, {
  targetKey: "id",
  foreignKey: "product_id",
  as: "product",
});

Product.hasMany(ProductImage, {
  sourceKey: "id",
  foreignKey: "product_id",
  as: "product_images",
});
ProductImage.belongsTo(Product, {
  targetKey: "id",
  foreignKey: "product_id",
  as: "product",
});

export { Product };
