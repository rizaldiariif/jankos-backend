import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface PageContentAttributes {
  id: string;
  page: string;
  key: string;
  type: string;
  label: string;
  content: string;
  file?: string | null;
  createdAt: Date;
  updatedAt: Date;
}

export interface PageContentCreationAttributes
  extends Optional<
    PageContentAttributes,
    "id" | "file" | "createdAt" | "updatedAt"
  > {}

class PageContent
  extends Model<PageContentAttributes, PageContentCreationAttributes>
  implements PageContentAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public page!: string;
  public key!: string;
  public type!: string;
  public label!: string;
  public content!: string;
  public file!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

PageContent.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    page: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    label: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    file: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "page_content",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { PageContent };
