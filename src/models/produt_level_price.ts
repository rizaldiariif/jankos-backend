import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { MemberLevel } from "./member_level";
import { Order } from "./order";
import { OrderStatusHistory } from "./order_status_history";
import { Product } from "./product";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ProductLevelPriceAttributes {
  id: string;
  product_id: string;
  member_level_id: string;
  price: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface ProductLevelPriceCreationAttributes
  extends Optional<
    ProductLevelPriceAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class ProductLevelPrice
  extends Model<
    ProductLevelPriceAttributes,
    ProductLevelPriceCreationAttributes
  >
  implements ProductLevelPriceAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public product_id!: string;
  public member_level_id!: string;
  public price!: number;
  public receiver_name!: string;
  public receiver_phone!: string;

  // Associations
  public getProduct!: BelongsToGetAssociationMixin<Product>;
  public readonly product?: Product;

  public getMemberLevel!: BelongsToGetAssociationMixin<MemberLevel>;
  public readonly member_level?: MemberLevel;

  public static associations: {
    product: Association<ProductLevelPrice, Product>;
    member_level: Association<ProductLevelPrice, MemberLevel>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

ProductLevelPrice.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    member_level_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "product_level_price",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS

// ASSOCIATIONS

export { ProductLevelPrice };
