import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Product } from "./product";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface OrderDetailAttributes {
  id: string;
  order_id: string;
  product_id: string;
  name: string;
  width: number;
  height: number;
  length: number;
  weight: number;
  final_weight: number;
  description: string;
  thumbnail: string;
  quantity: number;
  price: number;
  final_price: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface OrderDetailCreationAttributes
  extends Optional<OrderDetailAttributes, "id" | "createdAt" | "updatedAt"> {}

class OrderDetail
  extends Model<OrderDetailAttributes, OrderDetailCreationAttributes>
  implements OrderDetailAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public order_id!: string;
  public product_id!: string;
  public name!: string;
  public width!: number;
  public height!: number;
  public length!: number;
  public weight!: number;
  public final_weight!: number;
  public description!: string;
  public thumbnail!: string;
  public quantity!: number;
  public price!: number;
  public final_price!: number;

  // Associations
  public getProduct!: BelongsToGetAssociationMixin<Product>;
  public setProduct!: BelongsToSetAssociationMixin<Product, string>;
  public readonly product?: Product;

  public static associations: {
    product: Association<OrderDetail, Product>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

OrderDetail.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    width: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    height: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    length: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    weight: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    final_weight: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    final_price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "order_detail",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS

export { OrderDetail };
