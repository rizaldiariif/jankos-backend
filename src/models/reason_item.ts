import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ReasonItemAttributes {
  id: string;
  title: string;
  thumbnail: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ReasonItemCreationAttributes
  extends Optional<ReasonItemAttributes, "id" | "createdAt" | "updatedAt"> {}

class ReasonItem
  extends Model<ReasonItemAttributes, ReasonItemCreationAttributes>
  implements ReasonItemAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public title!: string;
  public thumbnail!: string;
  public text!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

ReasonItem.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "reason_item",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { ReasonItem };
