import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Admin } from "./admin";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface AdminLevelAttributes {
  id: string;
  name: string;
  hierarchy_number: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface AdminLevelCreationAttributes
  extends Optional<
    AdminLevelAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class AdminLevel
  extends Model<AdminLevelAttributes, AdminLevelCreationAttributes>
  implements AdminLevelAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public hierarchy_number!: number;

  // Associations
  public getAdmins!: HasManyGetAssociationsMixin<Admin>;
  public addAdmin!: HasManyAddAssociationMixin<Admin, string>;
  public hasAdmin!: HasManyHasAssociationMixin<Admin, string>;
  public countAdmin!: HasManyCountAssociationsMixin;
  public createAdmin!: HasManyCreateAssociationMixin<Admin>;
  public readonly admins?: Admin[];

  public static associations: {
    admins: Association<AdminLevel, Admin>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

AdminLevel.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    hierarchy_number: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "admin_level",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS
AdminLevel.beforeDestroy(async (admin_level, _options) => {
  // Revert Current Attached Admin to Basic Admin Level
  const basic_admin_level = await AdminLevel.findOne({
    where: { hierarchy_number: 1 },
  });

  if (basic_admin_level) {
    await Admin.update(
      {
        admin_level_id: basic_admin_level.id,
      },
      {
        where: {
          admin_level_id: admin_level.id,
        },
      }
    );
  }
});

// ASSOCIATIONS

export { AdminLevel };
