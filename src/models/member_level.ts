import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";
import { MemberLevelHistory } from "./member_level_history";
import { ProductLevelPrice } from "./produt_level_price";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberLevelAttributes {
  id: string;
  name: string;
  minimum_order: number;
  hierarchy_number: number;
  commision_percentage: number;
  price_cut_percentage: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface MemberLevelCreationAttributes
  extends Optional<
    MemberLevelAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class MemberLevel
  extends Model<MemberLevelAttributes, MemberLevelCreationAttributes>
  implements MemberLevelAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public minimum_order!: number;
  public hierarchy_number!: number;
  public commision_percentage!: number;
  public price_cut_percentage!: number;

  // Associations
  public getMembers!: HasManyGetAssociationsMixin<Member>;
  public addMember!: HasManyAddAssociationMixin<Member, string>;
  public hasMember!: HasManyHasAssociationMixin<Member, string>;
  public countMember!: HasManyCountAssociationsMixin;
  public createMember!: HasManyCreateAssociationMixin<Member>;
  public readonly members?: Member[];

  public getMemberLevelHistories!: HasManyGetAssociationsMixin<MemberLevelHistory>;
  public addMemberLevelHistory!: HasManyAddAssociationMixin<
    MemberLevelHistory,
    string
  >;
  public hasMemberLevelHistory!: HasManyHasAssociationMixin<
    MemberLevelHistory,
    string
  >;
  public countMemberLevelHistory!: HasManyCountAssociationsMixin;
  public createMemberLevelHistory!: HasManyCreateAssociationMixin<MemberLevelHistory>;
  public readonly member_level_histories?: MemberLevelHistory[];

  public static associations: {
    members: Association<MemberLevel, Member>;
    member_level_histories: Association<MemberLevel, MemberLevelHistory>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

MemberLevel.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    minimum_order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    hierarchy_number: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    commision_percentage: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    price_cut_percentage: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "member_level",
    sequelize: db,
    paranoid: true,
  }
);

// ASSOCIATIONS
MemberLevel.hasMany(ProductLevelPrice, {
  sourceKey: "id",
  foreignKey: "member_level_id",
  as: "product_level_prices",
});
ProductLevelPrice.belongsTo(MemberLevel, {
  targetKey: "id",
  foreignKey: "member_level_id",
  as: "member_level",
});

export { MemberLevel };
