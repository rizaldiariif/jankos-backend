import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ArticleAttributes {
  id: string;
  title: string;
  author: string;
  slug: string;
  thumbnail: string;
  text: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ArticleCreationAttributes
  extends Optional<
    ArticleAttributes,
    "id" | "slug" | "createdAt" | "updatedAt"
  > {}

class Article
  extends Model<ArticleAttributes, ArticleCreationAttributes>
  implements ArticleAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public title!: string;
  public author!: string;
  public slug!: string;
  public thumbnail!: string;
  public text!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Article.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    author: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "article",
    sequelize: db,
  }
);

// HOOKS
Article.beforeValidate(async (article, _options) => {
  if (article.changed("title")) {
    const slugified = article.title
      .toLowerCase()
      .replace(/ /g, "-")
      .replace(/[^\w-]+/g, "");
    article.slug = slugified;
  }
});

// ASSOCIATIONS
export { Article };
