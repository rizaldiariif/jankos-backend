import {
  Association,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManyHasAssociationMixin,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { MemberLevel } from "./member_level";
import { OrderDetail } from "./order_detail";
import { Product } from "./product";
import { ProductLevelPrice } from "./produt_level_price";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ProductCategoryAttributes {
  id: string;
  name: string;
  slug: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface ProductCategoryCreationAttributes
  extends Optional<
    ProductCategoryAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class ProductCategory
  extends Model<ProductCategoryAttributes, ProductCategoryCreationAttributes>
  implements ProductCategoryAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public name!: string;
  public slug!: string;

  // Associations
  public getProducts!: HasManyGetAssociationsMixin<Product>;
  public addProduct!: HasManyAddAssociationMixin<Product, string>;
  public hasProduct!: HasManyHasAssociationMixin<Product, string>;
  public countProduct!: HasManyCountAssociationsMixin;
  public createProduct!: HasManyCreateAssociationMixin<Product>;
  public readonly products?: Product[];

  public static associations: {
    products: Association<ProductCategory, Product>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

ProductCategory.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "product_category",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS

// ASSOCIATIONS
ProductCategory.hasMany(Product, {
  sourceKey: "id",
  foreignKey: "product_category_id",
  as: "products",
});
Product.belongsTo(ProductCategory, {
  targetKey: "id",
  foreignKey: "product_category_id",
  as: "product_category",
});

export { ProductCategory };
