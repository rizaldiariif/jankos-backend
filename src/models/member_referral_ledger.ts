import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { MemberReferral } from "./member_referral";
import { Order } from "./order";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberReferralLedgerAttributes {
  id: string;
  member_referral_id: string;
  order_id: string;
  amount: number;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberReferralLedgerCreationAttributes
  extends Optional<
    MemberReferralLedgerAttributes,
    "id" | "order_id" | "createdAt" | "updatedAt"
  > {}

class MemberReferralLedger
  extends Model<
    MemberReferralLedgerAttributes,
    MemberReferralLedgerCreationAttributes
  >
  implements MemberReferralLedgerAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_referral_id!: string;
  public order_id!: string;
  public amount!: number;

  // Associations
  public getMemberReferral!: BelongsToGetAssociationMixin<MemberReferral>;
  public readonly member_referral?: MemberReferral;

  public getOrder!: BelongsToGetAssociationMixin<Order>;
  public readonly order?: Order;

  public static associations: {
    member_referral: Association<MemberReferral, MemberReferralLedger>;
    order: Association<Order, MemberReferralLedger>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberReferralLedger.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_referral_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    order_id: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_referral_ledger",
    sequelize: db,
  }
);

export { MemberReferralLedger };
