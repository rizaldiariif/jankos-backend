import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MarketingMaterialAttributes {
  id: string;
  title: string;
  thumbnail: string;
  text: string;
  suggestion_date: Date;
  code: "product" | "promotion";
  createdAt: Date;
  updatedAt: Date;
}

interface MarketingMaterialCreationAttributes
  extends Optional<
    MarketingMaterialAttributes,
    "id" | "suggestion_date" | "createdAt" | "updatedAt"
  > {}

class MarketingMaterial
  extends Model<
    MarketingMaterialAttributes,
    MarketingMaterialCreationAttributes
  >
  implements MarketingMaterialAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public title!: string;
  public thumbnail!: string;
  public suggestion_date!: Date;
  public code!: "product" | "promotion";
  public text!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MarketingMaterial.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    suggestion_date: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Date.now(),
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "marketing_material",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { MarketingMaterial };
