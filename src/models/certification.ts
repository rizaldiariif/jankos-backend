import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface CertificationAttributes {
  id: string;
  thumbnail: string;
  createdAt: Date;
  updatedAt: Date;
}

interface CertificationCreationAttributes
  extends Optional<CertificationAttributes, "id" | "createdAt" | "updatedAt"> {}

class Certification
  extends Model<CertificationAttributes, CertificationCreationAttributes>
  implements CertificationAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public thumbnail!: string;

  // Associations

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Certification.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "certification",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { Certification };
