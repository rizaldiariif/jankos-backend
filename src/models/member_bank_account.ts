import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Member } from "./member";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface MemberBankAccountAttributes {
  id: string;
  member_id: string;
  bank_name: string;
  bank_account_name: string;
  bank_account_number: string;
  createdAt: Date;
  updatedAt: Date;
}

interface MemberBankAccountCreationAttributes
  extends Optional<
    MemberBankAccountAttributes,
    "id" | "createdAt" | "updatedAt"
  > {}

class MemberBankAccount
  extends Model<
    MemberBankAccountAttributes,
    MemberBankAccountCreationAttributes
  >
  implements MemberBankAccountAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public member_id!: string;
  public bank_name!: string;
  public bank_account_name!: string;
  public bank_account_number!: string;

  // Associations
  public getMember!: BelongsToGetAssociationMixin<Member>;
  public readonly member?: Member;

  public static associations: {
    member: Association<Member, MemberBankAccount>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

MemberBankAccount.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    member_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    bank_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bank_account_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bank_account_number: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "member_bank_account",
    sequelize: db,
  }
);

export { MemberBankAccount };
