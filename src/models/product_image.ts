import {
  Association,
  BelongsToGetAssociationMixin,
  DataTypes,
  Model,
  Optional,
} from "sequelize";
import { db } from "../config/db";
import { Product } from "./product";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface ProductImageAttributes {
  id: string;
  product_id: string;
  url: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

interface ProductImageCreationAttributes
  extends Optional<
    ProductImageAttributes,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
  > {}

class ProductImage
  extends Model<ProductImageAttributes, ProductImageCreationAttributes>
  implements ProductImageAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public product_id!: string;
  public url!: string;
  public receiver_name!: string;
  public receiver_phone!: string;

  // Associations
  public getProduct!: BelongsToGetAssociationMixin<Product>;
  public readonly product?: Product;

  public static associations: {
    product: Association<ProductImage, Product>;
  };

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

ProductImage.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE,
  },
  {
    tableName: "product_image",
    sequelize: db,
    paranoid: true,
  }
);

// HOOKS

// ASSOCIATIONS

export { ProductImage };
