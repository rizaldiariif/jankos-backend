export default (verification_token: string) => {
  return `
    <div style="
      height: auto;
      width: 100%;
      position: relative;
      padding-bottom: 100px;
      padding-top: 25px;
      background-color: #F29F05;
      border-radius: 4px;
    ">
      <div style="
        width: 75%;
        margin: 0px auto;
        padding: 24px;
        background-color: #ffffff;
        border-radius: 4px;
      ">
        <img src="https://cms.inaproducts.truevindo.co.id/uploads/logo_color_8d0203a39a.png" style="height: 100px; width:auto; margin-bottom: 16px;">
        <a href="${process.env.HOST}/api/visitors/verify-account?token=${verification_token}"
        style="
          display: block;
          padding: 8px 14px;
          border-radius: 4px;
          margin-bottom: 15px;
          background-color: #F29F05;
          box-shadow: 0px 4px 24px 1px rgba(0,0,0,0.3);
          -webkit-box-shadow: 0px 4px 24px 1px rgba(0,0,0,0.3);
          -moz-box-shadow: 0px 4px 24px 1px rgba(0,0,0,0.3);
          color: #ffffff;
          font-weight: bold;
          width: min-content;
          white-space: nowrap;
          text-decoration: none;
        "
        >Click Here to Verify</a>
        <p>Or you can copy this link to your browser.</p>
        <br/>
        <p>${process.env.HOST}/api/visitors/verify-account?token=${verification_token}</p>
      </div>
    </div>
  `;
};
