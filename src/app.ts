import express, { Request, Response } from "express";
import "express-async-errors";
import cors from "cors";
import dotenv from "dotenv";
import path from "path";

import { errorHandler } from "./middlewares/error-handler";

import { NotFoundError } from "./errors/not-found-error";

import { memberRouter } from "./routes/member";
import { memberLevelRouter } from "./routes/member_level";
import { productRouter } from "./routes/product";
import { flowRouter } from "./routes/flow";
import { check_payment } from "./scheduler/check_payment";
import { demoRouter } from "./routes/demo";
import { marketingMaterialRouter } from "./routes/marketing_material";
import { adminRouter } from "./routes/admin";
import { orderRouter } from "./routes/order";
import { articleRouter } from "./routes/article";
import { adminLevelRouter } from "./routes/admin_level";
import { memberReferralLedgerRouter } from "./routes/member_referral_ledger";
import { memberReferralRouter } from "./routes/member_referral";
import { testimonyRouter } from "./routes/testimony";
import { pageContentRouter } from "./routes/page_content";
import { productCategoryRouter } from "./routes/product_categories";
import { memberWithdrawalLedgerRouter } from "./routes/member_withdrawal_ledger";
import { reasonItemRouter } from "./routes/reason_item";
import { bannerHomeRouter } from "./routes/banner_home";
import { productImageRouter } from "./routes/product_image";
import { certificationRouter } from "./routes/certification";
import { engine } from "express-handlebars";
import { check_shipping } from "./scheduler/check_shipping";

dotenv.config();

const app = express();

// Set Middlewares
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors({ origin: true }));

// Set View Engine
app.engine("handlebars", engine());
app.set("view engine", "handlebars");
app.set("views", "./views");

// Set Routes
app.use("/uploads", express.static(path.join(__dirname, "/../uploads")));

app.use("/api/members", memberRouter);
app.use("/api/member-levels", memberLevelRouter);
app.use("/api/products", productRouter);
app.use("/api/product-categories", productCategoryRouter);
app.use("/api/orders", orderRouter);
app.use("/api/marketing-materials", marketingMaterialRouter);
app.use("/api/testimonies", testimonyRouter);
app.use("/api/banner-homes", bannerHomeRouter);
app.use("/api/certifications", certificationRouter);
app.use("/api/product-images", productImageRouter);
app.use("/api/reason-items", reasonItemRouter);
app.use("/api/page-contents", pageContentRouter);
app.use("/api/articles", articleRouter);
app.use("/api/admin-levels", adminLevelRouter);
app.use("/api/member-referrals", memberReferralRouter);
app.use("/api/member-referral-ledgers", memberReferralLedgerRouter);
app.use("/api/member-withdrawal-ledgers", memberWithdrawalLedgerRouter);
app.use("/api/flow", flowRouter);
app.use("/api/admins", adminRouter);
app.use("/api/demo", demoRouter);

// Set Route Not Found Error
app.all("*", async (_req: Request, _res: Response) => {
  throw new NotFoundError();
});

// Set Custom Error Handler
app.use(errorHandler);

// Schedulers
// check_payment();
check_shipping();

export { app };
