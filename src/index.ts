import { app } from "./app";
import { db } from "./config/db";
import dotenv from "dotenv";

const start = async () => {
  // Load env variables
  dotenv.config();

  // ENV VARIABLES CHECKING
  if (!process.env.APP_HOST) {
    throw new Error("HOST must be defined.");
  }

  try {
    // Connect to Database
    await db.authenticate();
    console.log("Connected to Database.");
  } catch (error) {
    console.error(error);
  }

  app.listen(process.env.PORT || 5000, () => {
    console.log("Listening to port 5000.");
  });
};

start();
