import cron from "node-cron";
import { BadRequestError } from "../errors/bad-request-error";
import { midtransClient } from "../helpers/api/midtrans";
import { shipOrder } from "../helpers/api/shipdeo";
import { Member } from "../models/member";
import { MemberPointLedger } from "../models/member_point_ledger";
import { MemberReferral } from "../models/member_referral";
import { MemberReferralLedger } from "../models/member_referral_ledger";
import { OrderDetail } from "../models/order_detail";
import { OrderStatus } from "../models/order_status";
import nodemailer from "nodemailer";
import { MailOptions } from "nodemailer/lib/json-transport";
import { generate_user_invoice_email } from "../helpers/emails/user_invoice_email";
import { Order } from "../models/order";
import { format } from "date-fns";
import shortUUID from "short-uuid";
import { MemberLevel } from "../models/member_level";
import { Op } from "sequelize";

export const check_payment = async () => {
  cron.schedule("0 */5 * * * *", async () => {
    console.log("==========================");
    console.log("Checking Order Payment");

    const unix_now = new Date().getTime() / 1000;

    const order_status_menunggu_pembayaran = await OrderStatus.findOne({
      where: { code: "menunggu_pembayaran" },
      include: [
        { association: "orders", include: ["order_details", "order_status"] },
      ],
    });
    const order_status_expired = await OrderStatus.findOne({
      where: { code: "expired" },
    });
    const order_status_processing = await OrderStatus.findOne({
      where: { code: "processing" },
    });

    if (!order_status_menunggu_pembayaran) {
      console.log("Order Status Menunggu is invalid!");
      return;
    }

    if (!order_status_expired) {
      console.log("Order Status Expired is invalid!");
      return;
    }

    if (!order_status_processing) {
      console.log("Order Status Processing is invalid!");
      return;
    }

    if (!(order_status_menunggu_pembayaran.orders instanceof Array)) {
      console.log(order_status_menunggu_pembayaran.orders);
      console.log("No Pending Payment Order!");
      return;
    }

    for (let order of order_status_menunggu_pembayaran.orders) {
      try {
        const midtrans_response = await midtransClient.get(
          `/v2/${order.id}/status`
        );

        switch (midtrans_response.data.transaction_status) {
          case "expire":
            order.set("order_status_id", order_status_expired.id);
            await order.save();
            // @ts-ignore
            order = await Order.findByPk(order.id, {
              include: ["order_details", "order_status"],
            });
            break;
          case "settlement":
            order.set("order_status_id", order_status_processing.id);
            await order.save();
            // @ts-ignore
            order = await Order.findByPk(order.id, {
              include: [
                "order_details",
                "order_status",
                { association: "member", include: ["member_level"] },
              ],
            });

            // CHECK FOR UPDATE MEMBER LEVEL
            // CHECK IF LEVEL IS ALREADY DIAMOND
            if (
              order.member?.member_level_id !=
              "dba6e228-4ba6-4259-b7f7-3deb35987d57"
            ) {
              const total_quantity = order.order_details
                ?.map((od) => od.quantity)
                .reduce((old_q, current_q) => old_q + current_q, 0) as number;

              // CHECK FOR BEST NEXT LEVEL
              const next_member_level = await MemberLevel.findOne({
                where: {
                  minimum_order: {
                    [Op.gt]: order.member?.member_level?.minimum_order,
                    [Op.lte]: total_quantity,
                  },
                },
                order: [["minimum_order", "DESC"]],
              });

              if (next_member_level) {
                if (total_quantity >= next_member_level.minimum_order) {
                  order.member?.set("member_level_id", next_member_level.id);
                  await order.member?.save();
                }
              }
            }

            break;
          default:
            // CHECK IF ORDER IS ALREADY CREATED IN MORE THAN 1 HOURS
            const unix_created_order = order.createdAt.getTime() / 1000;
            if (unix_now - unix_created_order > 86400) {
              order.set("order_status_id", order_status_expired.id);
              await order.save();
              // @ts-ignore
              order = await Order.findByPk(order.id, {
                include: ["order_details", "order_status"],
              });
            }
            break;
        }

        if (
          midtrans_response.data.transaction_status == "expire" ||
          midtrans_response.data.transaction_status == "settlement"
        ) {
          if (typeof process.env.NODEMAILER_USER !== "string") {
            throw new Error("NODEMAILER_USER must be defined.");
          }
          if (typeof process.env.NODEMAILER_PASS !== "string") {
            throw new Error("NODEMAILER_PASS must be defined.");
          }

          const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: process.env.NODEMAILER_USER,
              pass: process.env.NODEMAILER_PASS,
            },
          });

          const mailOptions = {
            from: process.env.NODEMAILER_USER,
            to: order.contact_email,
            subject: `[Jankos Glow] - INVOICE - INV/${format(
              new Date(order.createdAt),
              "yyyy/MM/dd"
            )}/${shortUUID().fromUUID(order.id).toUpperCase()}`,
            html: generate_user_invoice_email(order),
          } as MailOptions;

          await transporter.sendMail(mailOptions);
        }
        // Create Shipdeo Shipping Data
        if (order.order_status_id === order_status_processing.id) {
          await shipOrder(order);

          const order_details = await OrderDetail.findAll({
            where: { order_id: order.id },
          });

          const total_price = order_details
            .map(
              (order_detail) => order_detail.final_price * order_detail.quantity
            )
            .reduce(
              (total_current_price, current_price) =>
                total_current_price + current_price
            );

          // Point
          let point = total_price * 0.1;
          if (!order.payment_url) {
            point = point * -1 * 10;
          }
          await MemberPointLedger.create({
            member_id: order.member_id,
            order_id: order.id,
            amount: point,
          });

          // Referral
          // const member_referral = await MemberReferral.findOne({
          //   where: { referred_member_id: order.member_id },
          //   include: [
          //     {
          //       association: "upline_member",
          //       include: ["member_level"],
          //     },
          //   ],
          // });

          // if (
          //   member_referral &&
          //   member_referral.upline_member &&
          //   member_referral.upline_member.member_level
          // ) {
          //   if (total_price) {
          //     await MemberReferralLedger.create({
          //       amount:
          //         total_price /
          //         member_referral.upline_member.member_level
          //           .commision_percentage,
          //       member_referral_id: member_referral.id,
          //       order_id: order.id,
          //     });
          //   }
          // }
        }
      } catch (error) {
        console.log(error);
      }
    }

    console.log("==========================");
  });
};
