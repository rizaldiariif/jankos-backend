import cron from "node-cron";
import { Op } from "sequelize";
import { BadRequestError } from "../errors/bad-request-error";
import { shipdeoClient } from "../helpers/api/shipdeo";
import { Order } from "../models/order";
import { OrderStatus } from "../models/order_status";

export const check_shipping = async () => {
  cron.schedule("0 */1 * * * *", async () => {
    console.log("==========================");
    console.log("Checking Order Shipping");

    try {
      const order_statuses = await OrderStatus.findAll({
        where: {
          code: {
            [Op.in]: [
              "request_pickup",
              "processing",
              "picked",
              "delivering",
              "lost_broken",
            ],
          },
        },
      });

      const order_status_ids = order_statuses.map(
        (order_status) => order_status.id
      );

      const orders = await Order.findAll({
        where: {
          order_status_id: {
            [Op.in]: order_status_ids,
            [Op.ne]: null,
          },
        },
        include: ["order_status"],
      });

      const shipdeo_client = await shipdeoClient();

      for (const order of orders) {
        if (!order.order_status) {
          throw new BadRequestError("Order Status is not valid!");
        }

        if (!order.airwaybill_number) {
          continue;
        }

        try {
          const { data } = await shipdeo_client.post(`/v1/couriers/waybill`, {
            courier_code: order.shipping_courier_code,
            waybill: order.airwaybill_number,
          });

          const current_order_status = await OrderStatus.findOne({
            where: { code: data.data.delivery_status.status.toLowerCase() },
          });

          if (
            current_order_status &&
            order.order_status.code !==
              data.data.delivery_status.status.toLowerCase()
          ) {
            order.set("order_status_id", current_order_status.id);
            await order.save();
          }
        } catch (error) {
          console.error(error);
          continue;
        }
      }
    } catch (error) {
      console.log(error);
    }

    console.log("==========================");
  });
};
